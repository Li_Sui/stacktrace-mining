INTRODUCTION
------------

This is the repository for project `An Investigation into the Unsoundness of Static Program Analysis`.

Spikes may include tools:

* using for static program analysis `Soot`, `Wala`, `Doop`
* using for dynamic program analysis `AspectJ`(may change to use [Xcorpus](Link https://bitbucket.org/jensdietrich/corpus-x) in the future)


ENVIRONMENT
-----------

*  ant:1.9.7 set `ANT_HOME` to point to the ANT installation root folder
*  ivy:2.4.0 install by copying jar into `$ANT_HOME/lib`
*  JDK:1.7 and JDK 1.8
*  Doop: dowload from [here](Link https://bitbucket.org/yanniss/doop). checkout the tag `centauri20161012`


HOW TO
-----------
`Doop` setup:
install [PA-Datalog](Link http://snf-705535.vm.okeanos.grnet.gr/agreement.html).
run `source /opt/lb/pa-datalog/lb-env-bin.sh` and configure following:

```
#!java

export LOGICBLOX_HOME=PATH_TO_YOUR_LOGICBLOX
export DOOP_HOME=PATH_TO_YOUR_DOOP
export PATH=$DOOP_HOME/bin:$LOGICBLOX_HOME/bin:$PATH
```
example commands to produce callgraph with reflection features enabled(this will be facilitate later with ANT)

```
#!java


./doop -a context-insensitive --enable-reflection -i PATH_TO_JAR --platforms-lib  --platform java_7  --Xstats:none

bloxbatch -db last-analysis -print ReflectiveMethodInvocation
```




--------------------------------------------------------------------------------------------------------

`JDK7_HOME` need to be configured in `commons.properties`. `soot` does not support JDK1.8


run `ant soot-callgraph` with either `-Dexample=reflection` or `-Dexample=dynamicproxy` or `-Dexample=serialisation` to generate call graph by using soot.


run `ant wala-callgraph` with either `-Dexample=basic(1-4)` or other reflection examples(reflection1-9) or `-Dexample=lambda` or `-Dexample=dynamicproxy` or `-Dexample=serialisation` or `-Dexample=serviceloader` to generate call graph by using wala.


Other available options:


`ant wala-all` and `ant soot-all` will run all examples


`ant wala-dynamo` will apply [dynamo](https://bitbucket.org/kjezek/dynamo) to transform bytecode, test the invokedynamic behaviour using wala