
\documentclass[runningheads,a4paper]{llncs}

\usepackage{todonotes}
\usepackage{multirow}
\usepackage{listings}
\usepackage{url}
\usepackage{hyperref}
\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\ttfamily\scriptsize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{black},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{black},       % keyword style
	language=Octave,                 % the language of the code
	otherkeywords={*,...},           % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{black}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{black},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{On the Use of Mined Stack Traces to Improve the Soundness of Statically Constructed Call Graphs}
%Alternative title: Mining stack traces to imporve the soundess of statically constructed call graphs

\author{Li Sui \and Jens Dietrich \and Amjed Tahir}
% author names and affiliations
% use a multiple column layout for up to two different
% affiliations
\institute{School of Engineering and Advanced Technology, Massey University, Palmerston North, New Zealand\\
\email{leesui0207@gmail.com},  \email{\{j.b.dietrich,a.tahir\}@massey.ac.nz}}
% disabled for blind review
%\author{\IEEEauthorblockN{Li Sui, Jens Dietrich, Amjed Tahir}
%\IEEEauthorblockA{School of Engineering and Advanced Technology, Massey University, Palmerston North, New Zealand\\
%Email: leesui0207@gmail.com,\{j.b.dietrich,a.tahir\}@massey.ac.nz}
%}

\maketitle

\begin{abstract}
Static program analysis is a cornerstone of modern software engineering -- it is used to detect bugs and security vulnerabilities early before software is deployed. While there is a large body of research into the scalability and the precision of static analysis, the (un)soundness of static analysis is a critical issue that has not attracted the same level of attention by the research community. 

In this paper we explore whether information harvested from stack traces obtained from GitHub issue tracker and Stack Overflow Q\&A forum can be used in order to complement statically built call graphs. For this purpose, we extract reflective call graph edges from parsed stack traces, and check whether these edges are correctly computed by \textit{Doop}, a widely used tool for static analysis. We find several edges that \textit{Doop} misses when analysing real-world programs. This suggests that mining techniques are a useful tool to test and improve the soundness of static analysis. 

\end{abstract}

\keywords{
stack trace analysis, static analysis, empirical studies, soundness, call graph construction,
mining software repositories
}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.



\section{Introduction}

\section{Methodology}

We conducted experiments on the \textit{DaCapo 2009} data set~\cite{blackburn2006dacapo}, a Java benchmark that is widely used in programming language research. We augmented this dataset with several additional programs that are known to use reflection and appeared the most from mined stack trace dataset. \todo{is it ok to mention this?}

We investigated the use of APIs and replicated datasets to mine repository data, in particular the GitHub API \footnote{\url{https://api.github.com/} (accessed 17 January 2017)} and the GHTorent project~\cite{gousios2013ghtorent} that collects and publishes GitHub snapshots. Both had limitations that have rendered them as unsuitable for our study. The GitHub API does not support fine-grained text search, we are interested to analyse pages from the issue tracking system containing the string \texttt{java.lang.reflect.InvocationTargetException}. GHTorent does not store the bodies of the actual issues, but only the respective metadata. 

We therefore decided to  write a custom HTML client to performed searches on the GitHub and Stack Overflow Q\&A web sites for issues and discussions including the text \texttt{java.lang.reflect.InvocationTargetException}
%search string: \texttt{is:issue java.lang.reflect.InvocationTargetException} 
%URL: \url{https://github.com/search?utf8=%E2%9C%93&q=is%3Aissue+java.lang.reflect.InvocationTargetException}

The client mimics a web browser session using the appropriate headers, and returns the URLs of the respective static issue web pages. We then downloaded the respective web pages, stripped HTML markup and extracted stack traces, instantiating the meta-model depicted in figure \ref{fig:metamodel}. We also applied a filter to remove stack traces not containing any of the classes from our dataset. We extracted the respective class list using bytecode analysis on the respective programs. Then we inferred edges representing reflective method invocations from stack traces containing ``caused by'' clauses, as discussed above.  The web scraps were performed on 19 January 2017.

Stack trace is lack of versioning information. This could produce false positives. Stack trace contains line number indicating where the method declaration is(e.g.Foo.main(Foo.java:3)). Therefore, we use line number provided by stack trace, to match the one we found in bytecode across different version. First, we download all relevant jar files from MAVEN repository\footnote{The download script can be found \url{https://bitbucket.org/jensdietrich/evilpickles}}. Then use ASM\footnote{\url{http://asm.ow2.org/}}, a bytecode analysis framework, to obtain line information. At the end, we pick the latest version that match with the line number in stack trace.

We also built static call graphs using \textit{Doop}, and checked whether any of the edges extracted were missing from these call graphs. The complete process is depicted in figure \ref{fig:process}. At the end of this process, we manually validated the results by investigating the respective source code. We also used \textit{Doop} with reflection flag on, to detect reflection.

\begin{figure}
	\includegraphics[width=9cm]{metamodel.pdf}
	\caption{Stacktrace metamodel}
	\label{fig:metamodel}	
\end{figure}

\begin{figure}
	\includegraphics[width=9cm]{process.pdf}
	\caption{Process to extract and analyse stacktrace data}
	\label{fig:process}	
\end{figure}



\section{Results}
List of selected program(addition to Dacapo), 10 programs with 13 identical source call sites. We found 495 edges accross 569 websites that matched those 13 source callsites. We pick the earliest and stable version as they match the line number in source call sites. 


\begin{table}[h]
\centering
\caption{My caption}
\label{my-label}
\begin{tabular}{|l|l|l|l|}
\hline
Name                     & Source Callsite                                                                      & Number of target callsites & Version \\ \hline
Exec-maven-plugin        & org.codehaus.mojo.exec.ExecJavaMojo\$1.run                                           & 189                        &  Exec-maven-plugin-1.2.1       \\ \hline
Spring-boot-loader       & org.springframework.boot.loader.MainMethodRunner.run                                 & 175                        &  spring-boot-loader-1.2.5.RELEASE      \\ \hline
Spring Boot Maven Plugin & org.springframework.boot.maven.RunMojo\$LaunchRunner.run                             & 33                         &  spring-boot-maven-plugin-1.2.0.RELEASE       \\ \hline
Gwt-dev                  & com.google.gwt.dev.shell.ModuleSpace.onLoad                                          & 28                         &  gwt-dev-2.1.1      \\ \hline
Surefire-api             & org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray                 & 19                         &  surefire-api-2.11      \\ \hline
weld-core-impl(jboss)    & org.jboss.weld.injection.producer.DefaultLifecycleCallbackInvoker.invokeMethods      & 20                         &  weld-core-impl-2.2.12.Final      \\ \hline
\multirow{3}{*}{Guava}   & com.google.common.eventbus.EventSubscriber.handleEvent                               & 4                          &  guava-16.0       \\ \cline{2-4} 
                         & com.google.common.eventbus.EventHandler.handleEvent                                  & 12                         &  guava-15.0       \\ \cline{2-4} 
                         & com.google.common.base.internal.Finalizer.cleanUp                                    & 1                          &  guava-11.0      \\ \hline
Antlr                    & org.antlr.v4.parse.GrammarTreeVisitor.visit                                          & 1                          &  antlr4-4.0      \\ \hline
Hadoop-hbase-client      & org.apache.hadoop.hbase.protobuf.ProtobufUtil.toFilter                               & 3                          &  hbase-client-0.98.0-hadoop1      \\ \hline
Log4j                    & org.apache.logging.log4j.core.config.plugins.util.PluginBuilder.build                & 9                         &  log4j-core-2.1    \\ \hline
Jasperreports            & net.sf.jasperreports.data.hibernate.HibernateDataAdapterService.contributeParameters & 1                          &  jasperreports-6.3.0       \\ \hline
\end{tabular}
\end{table}

\section{Conclusion}



\bibliographystyle{plain}
\bibliography{references}


% that's all folks
\end{document}


