
\documentclass[10pt, numbers]{sigplanconf}

\usepackage{todonotes}
\usepackage{listings}
\usepackage{url}
\usepackage{hyperref}
\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\ttfamily\scriptsize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{black},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{black},       % keyword style
	language=Octave,                 % the language of the code
	otherkeywords={*,...},           % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{black}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{black},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}
\title{On the Construction of Soundness Oracles}

\authorinfo
	{Jens Dietrich \and Li Sui \and Shawn Rasheed \and Amjed Tahir}
	{School of Engineering and Advanced Technology, Massey University, Palmerston North, New Zealand}
	{j.b.dietrich@massey.ac.nz, leesui0207@gmail.com, s.rasheed@massey.ac.nz, A.Tahir@massey.ac.nz}
\maketitle

\begin{abstract}

One of the inherent advantages of static analysis is that it can create and reason about models of an entire program. However, mainstream languages such as Java use numerous dynamic language features designed to boost programmer productivity, but these features are notoriously difficult to capture by static analysis, leading to unsoundness in practice. 

While existing research has focused on providing sound handling for selected language features (mostly reflection) based on anecdotal evidence and case studies, there is little empirical work to investigate the extent to which particular features cause unsoundness of static analysis in practice. In this paper, we (1) discuss language features that may cause unsoundness and (2) discuss a methodology that can be used to check the (un)soundness of a particular static analysis, call-graph construction, based on soundness oracles. These oracles can also be used for hybrid analyses.  
\end{abstract}

\section{Introduction}


Static analysis is used to build models of software and then to reason about these models in order to detect design flaws, bugs and vulnerabilities. An inherent advantage of static analysis is that it can be, at least in principle, sound: it can cover the entire program. In order to be useful, these models use abstractions, and this has an impact on their precision: they over-approximate actual program behaviour. Dynamic analysis on the other hand uses a driver (harness) to execute (exercise) the actual program and build models by observing this execution. It therefore only represents actual program behaviour, but misses behaviour not triggered by the driver. It is therefore precise, but inherently unsound.

Unfortunately, in practice, static analysis is not sound either. Languages like Java are full of dynamic features invented to boost programmer productivity and facilitate the design of frameworks. These features are notoriously difficult to capture in static analysis, and as a result, static analysis tools are unsound. What is more, attempts to improve soundness often cause a loss of precision, as features have to be modelled using over-approximation. The authors of the Soundiness Manifesto put it like this: \textit{``we are not aware of a single realistic whole-program analysis tool (for example, tools widely used for bug detection, refactoring assistance, programming automation, and so forth) that does not purposely make unsound choices''}~\cite{livshits2015defense}. 

Figure~\ref*{fig:soudness} illustrates this situation. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=8.5cm]{types-of-analysis.pdf}
	\caption{The (un)soundness of static program analysis}
	\label{fig:soudness}	
\end{figure}

While there is a significant body of work on improving the soundness of static analysis, mostly by improving the handling of reflection, there is little work on how to systematically study the soundness of static analysis tools and methods for real world programs. Such a study requires representative corpora of programs, and \textit{soundness oracles}: \textit{actual program behaviour} obtained by means other than static analysis. Such oracles can then be used to detect, catalogue and measure the gaps in static analysis.

In this paper, we discuss and compare several methods that can be used to construct such soundness oracles. Our work only considers one particular kind of program analysis: call graph construction. However, this is a foundational analysis widely used for bug and vulnerability detection, and intertwined with points-to / alias analysis (call graph construction on-the-fly~\cite{shivers1991control}).

We also present a list of language features that are potential sources of unsoundness. While many of these features have been discussed in previous research, there are some more exotic features that are not widely known. For those cases, we provide some examples illustrating the problems a static analysis would encounter. 

\section{Related Work}


There are two seminal papers that set the scene for our work: Ernst~\cite{ernst2003static} explored the \textit{``synergy and duality''} of static and dynamic analysis, and argued to combine them in order to \textit{``blend the strength of both approaches''}. Our approach adapts this idea by generating some (but not all) soundness oracles by means of dynamic analysis. More recently, Livshits et al. \cite{livshits2015defense} published the ``In Defense of Soundiness'' manifesto where they emphasise the need to make unsound choices. They then went on to issue \textit{``a call to the community to identify clearly the nature and extent of unsoundness in static analyses''}. This work is in direct response to this call.   

Research on improving the soundness of static analysis has focused on reflection handling. A good, up-to-date overview of work in this space is given by Landman et al~\cite{landman2017challenges}. We focus the discussion on certain core milestone papers. Livshits et al \cite{Livshits2005Reflection} have investigated support for reflection in the context of points-to analysis. They do this by associating objects with reflective call sites, and then track strings that are used as class names.
Type information from cast statements is used to improve precision, taking advantage of the fact that often cast statements are used at reflective instantiation sites. Smaragdakis et al~\cite{smaragdakis2015more} have improved this approach further by adding substring and string flow analysis. This work uses the \textit{doop} framework~\cite{bravenboer2009strictly}.

Bodden et al \cite{bodden2011taming} proposed \textit{Tamiflex} -- a tool to improve the soundness of static analysis with \textit{soot}~\cite{vallee1999soot}. \textit{Tamiflex} uses a hybrid analysis with additional information inferred from exercising the program under analysis. Therefore, the quality of the analysis strongly relies on the driver (harness) provided by the user. Bodden has also proposed support for \texttt{invokedynamic} in \textit{soot}~\cite{bodden2012invokedynamic}, although this only supports the parsing and representation of \texttt{invokedynamic}, not the actual reasoning. 

Sridharan et al. proposed the ``framework for frameworks''~\cite{sridharan2011f4f} -- an approach to deal with dynamic language features in frameworks. The idea is to improve the analysis by taking configuration files into account, given that many framework store data used in reflection (like class names) in files (deployment descriptors, component metadata, etc). Some programming tools implement a similar strategy, for instance, some refactoring browsers embedded in Integrated Development Environments (IDEs) will apply refactorings to framework files like \texttt{web.xml}. 

To summarise, these papers illustrate three different approaches to deal with the unsoundess problem, as follows: 
(1) make static analysis more expressive and try to model and reason about dynamic language features~\cite{Livshits2005Reflection,smaragdakis2015more}, (2) use a hybrid analysis~\cite{bodden2011taming}
or (3) use contextual information on how dynamic features are actually being used in programs~\cite{sridharan2011f4f} .


\section{Dynamic Language Features in Java}

\subsection{Overview}

We provide a brief overview of language features that are difficult to handle in static analysis tools. The list contains some of the usual suspects, such as reflection, plus some more exotic features not widely known. These features are not necessarily orthogonal. For instance, embedded scripting may use other dynamic features such as reflection, invokedynamic or dynamic class loading depending on whether scripts are interpreted or compiled. But treating it separately is warranted in our opinion as the semantics of scripts could be exploited by static analysis tools. 

\subsection{Reflection}

Java has a rich reflection API that is widely used in practice. There are several levels of reflection APIs in Java, from low level access to methods and fields, to libraries offering a higher level of abstraction like \texttt{java.beans.Introspector}. Reflection is often used for design patterns like service locator or dependency injection (Spring, OSGi DS, etc)~\cite{fowler2004inversion}, an example of such a mechanism with built-in platform support is \texttt{java.util.ServiceLoader} that locates ``services'' libraries advertised in component (library) meta data. 
%For instance, this used used in modern JDBC drivers.

\subsection{Class Loading}

Java programs use dedicated class loaders to initialise classes, which can be customised. Since the identity of classes is based on the fully qualified class name and the class loader used, class loaders provide a solution for DLL hell style problems: the co-existence of multiple inconsistent versions of the same class. This make the use of class loaders popular in module frameworks like OSGi where ``plugin'' components have private class paths. The problem for call graph construction is that (1) not all classes present at runtime are visible to static analysis, (2) not all of the possible actual types of objects are known, which is necessary for precise devirtualisation. 

Interestingly, class unloading is also possible and now widely used in enterprise servers, usually by using the OSGi platform supporting the unloading of bundles. The main application is to facilitate hot upgrades. Static analysis could also exploit this by reasoning about program behaviour that is unreachable as classes have been unloaded by then. However, this would only improve precision, not soundness. 

\subsection{Proxies}

Proxies are a language feature to ``fake'' interfaces by delegating calls to implemented methods to invocation handlers. They are the Java equivalent of Smalltalk's famous \texttt{doesNotUnderstand} protocol. The main original use case is the representation of remote objects (client stubs) as an alternative to the static generation of code from (Java or CORBA IDL) interfaces. But proxies have found multiple other applications, such as mock testing (\textit{Mockito}) or to implement aspect-oriented programming (AOP) (\textit{Spring}). 

The problems for static analysis caused by proxies are to correctly model the invocation handlers. 

\subsection{Serialization}

Object serialization is used to serialize object graphs, the main use cases are persistence, remoting and deep copying. There are two serialization mechanisms built into Java, binary and XML-based, plus numerous third party solutions. While XML based serialization internally uses reflection, binary serialization is directly supported by the JVM. When a program uses serialization, certain call sites become known only at runtime, and this behaviour is difficult to capture in call graph construction. Binary serialization can use special class loaders, for instance to load remote classes when used in RMI. 

\subsection{Instrumentation}

Instrumentation is the addition of bytecode. There are several use cases, including (1) gathering data, for instance, to be used by instrumenting profilers, and (2) adding behaviour, for instance, in order to address cross-cutting concerns in AOP~\cite{kiczales1997aspect}.

While early generation instrumentation was static, applied by a pre-- or post-compiler to either source- or bytecode respectively, modern instrumentation techniques can be applied late when classes are loaded. This causes problems for static analysis tools that do not have access to the instrumented code. 

\subsection{Native Methods and Incoming Calls}

Many Java programs contain native methods, and this is problematic for static analysis tools that cannot trace these calls. 

But the opposite is also true: native (or rather, external) code can invoke Java methods, using mechanisms such as RMI, CORBA or Protocol Buffers. An example for a complex, bi-directional communication with the platform is \texttt{java.nio.file.WatchService}. The documentation stipulates that \textit{``The implementation that observes events from the file system is intended to map directly on to the native file event notification facility where available, or to use a primitive mechanism, such as polling, when a native facility is not available.''} \footnote{\url{https://goo.gl/WivLgo} [accessed 12 March 17]}. This could include native method calls (when polling is used), or mechanisms where system functions call Java methods (when native file event notification is used).

\subsection{Unsafe}

Java contains the class \texttt{sun.misc.Unsafe} that provides an API for several low-level functions. There are ongoing attempts to restrict access to this class in Java 9, however, it turns out that it is still widely used in real-world programs~\cite{mastrangelo2015use}. As an example for how \texttt{Unsafe} can cause problems for static analysis, consider the code in figure~\ref{listing:unsafe}.

\begin{figure}
	\caption{Dynamic instantiation using \texttt{sun.misc.Unsafe}}
	\label{listing:unsafe}	
	\begin{lstlisting}[frame=single]
public class Foo {}
	
import sun.misc.Unsafe;
import java.lang.reflect.Field;
public class Main {
	public static void main(String[] args) throws Exception {
		Field f = Unsafe.class.getDeclaredField("theUnsafe");
		f.setAccessible(true);
		Unsafe unsafe = (Unsafe) f.get(null);
		Foo o = (Foo)unsafe.allocateInstance(Foo.class);
		System.out.println(o);
	}
}
\end{lstlisting}
\end{figure} 

First, the \texttt{Unsafe} singleton instance is accessed using reflection in order to avoid a \texttt{SecurityException} that restricts access to the instance. Then an instance of \texttt{Foo} is created using \texttt{allocateInstance}. Typically, static analysis tools recognise allocation sites by looking for a bytecode instruction \texttt{new , dup , invokespecial <init>}. However, in this case an instance is created with \texttt{invoke\-virtual sun/misc/Unsafe.allocateInstance}. Missing allocation sites may result in missing call graph edges if call graph construction on-the-fly~\cite{shivers1991control} is used. 

\subsection{Invokedynamic}

The \texttt{invokedynamic} instruction was introduced in Java 7 and is used by the Java compiler in Java 8 to compile lambdas. \texttt{invokedynamic} gives the user more control over the dispatch mechanism by resolving call targets at runtime via a user-defined bootstrap method. In Java 8, this is only used for the compilation of lambdas, and bootstrapping uses a fixed set of methods in \texttt{java.lang.invoke.Lambda\-Meta\-factory}. However, a none-standard Java compiler like \textit{dynamo}~\cite{jezek2016magic} or a compiler for a different JVM language may produce code that uses a different bootstrapping mechanisms, and therefore static analyses should not rely on the presence of the \texttt{LambdaMetafactory}.  

\subsection{Scripting and Runtime Compilation}

In recent years, domain-specific languages (DSL)~\cite{fowler2010domain} have become increasingly popular and are now widely used. Examples include embedded full-fledged Turing-complete languages like the Rhino ECMA Script engine embedded in Java, expression languages like \textit{JSP EL}, \textit{OGNL} and \textit{MVEL}, templating languages like \textit{velocity} and \textit{StringTemplate}, and query and transformation languages like \textit{sql}, \textit{xpath} and \textit{xslt}. 

When expressions in a language are evaluated, (Java) methods of the host program may be invoked. To illustrate this, consider the following example in figure \ref{listing:mvel}. 
\begin{figure}
	\caption{Expression compilation and evaluation in MVEL}
\label{listing:mvel}	
\begin{lstlisting}[frame=single]
public class Bean {
	public int getValue() {return 42;}
}
import org.mvel2.MVEL;                                                             
import java.util.*;                                                              	
public class Main {                                                                
	public static void main(String[] args) throws Exception {  
		String s = "bean.value==42"         
		Object x = MVEL.compileExpression(s);          
		Map b = new HashMap();                                          
		b.put("bean",new Bean());                                       
		boolean r = (Boolean)MVEL.executeExpression(x,b); 
		assert r;                                                         
	}                                                                          
}  
\end{lstlisting}
\end{figure} 

This shows how the MVEL expression and template engine compiles and evaluates an expression. According to the semantics of MVEL, \texttt{bean.value} will get interpreted as a method call to \texttt{getValue()}, and therefore, a sound call graph analysis tool needs to compute a path from \texttt{Main.main()} to \texttt{Bean.getValue()}.

To use DSLs at runtime, interpretation or compilation of DSL code is required. This can range from interpretation using Java reflection to full-fledged bytecode generation combined with classloading. For instance, the \textit{xalan-2.7.2} compiler \texttt{org.apache.xalan.xsltc.compiler.XSLTC}\footnote{\texttt{XSLTC} uses \texttt{GregorSamsa}~\cite{kafka2015metamorphosis} as name for generated classes.} generates bytecode using the \textit{bcel} library. 

Depending on whether scripts are interpreted or compiled, method calls may or may not  be reflective. But even if a compiler that produces bytecode was used, a static analysis would still have problems as it might not be able to access the code. It is for instance possible to compile completely in memory, without ever storing a class definition on a disk. 

There are standard APIs to facilitate the integration of DSLs (JSR223~\cite{jsr223}) and runtime compilation (JSR199~\cite{jsr199}). Examples of real-world libraries using these APIs are \textit{jmeter-3.1} (JSR223) and \textit{drools-7.0.0}  (JSR199). 


\section{Soundness Oracle Generation}

\subsection{Introduction}

Assuming that all static analysis tools encounter the same problems, such as ``how to deal with reflection'', soundness oracles should be generated by means other than static analysis. One could argue that there is some benefit in generating oracles with a static analysis tool A and use it to assess static analysis tool B, but we believe that there is limited benefit to this. 

When discussing methods to generate soundness oracles, we are interested in three aspects:

\begin{enumerate}
	\item \textbf{Size}: larger oracles are generally better, as they are more likely to cover the gaps.
	\item \textbf{Quality}: oracles should contain \textit{interesting behaviour}: program behaviour leading to bugs or vulnerabilities, or behaviour resulting from the use of dynamic language features and likely to be missed by static analysis.
	\item \textbf{Effort}: it should be easy to generate oracles, preferably, the process should be automated.
	
\end{enumerate}

In the context of call graph construction, such a soundness oracle contains call graph edges.  


\subsection{On-board Tests and Executables}

The obvious way to generate soundness oracles is by means of dynamic analysis. Programs are exercised starting with existing program entry points (\texttt{main(String[])} for standalone executables, \texttt{doGet(Http\-Servlet\-Request, Http\-Servlet\-Response)} for web applications, etc). This can be facilitated by crafting dedicated ``drivers'', this is the approach taken by \textit{DaCapo}~\cite{blackburn2006dacapo}. The executing program is then observed using injected code (instrumentation) or by acquiring thread dumps (sampling), and the oracle is built from the results of this observation.

The main problem with this approach is that it is difficult to find or create drivers that exercise a large percentage of relevant program behaviour. This can be approximated by measuring branch coverage: low coverage indicates that large parts of the program are never exercised. For instance, the average coverage obtained when using the \textit{DaCapo} driver is rather low (16.10\%)~\cite{xcorpus}.

A potentially better approach to exercise programs is the use of unit tests. The use of frameworks like \textit{junit} is common practice in many Java programs, and many software engineers are aware of coverage metrics and aim for high coverage.  Unfortunately, we found that the coverage obtained by running built-in tests on real world programs is still relatively low~\cite{xcorpus}, impacting the size of the oracle. On the other hand, the oracles generated with built-in tests should contain interesting and relevant program behaviour as programmers are more likely to write tests for functionality that uses complex features, in particular when this has caused problems in the past and test cases were created as part of documenting and fixing bugs in the spirit of test-driven development~\cite{beck2003test}. 

The effort needed to generate such an oracle is moderate. Test case execution is usually easy to automate, in particular as many projects use canonical project layouts and naming conventions (such as the \textit{Maven} project layout). To execute \texttt{main} methods and similar program entry points usually requires meaningful runtime parameters, and finding those can be challenging.    

\subsection{Generated Tests}

A viable alternative to existing tests is test case generation. There are several tools available for Java and other platforms. We have experimented with \textit{Evosuite}~\cite{fraser2011evosuite}, a test generation framework that used an evolutionary algorithm to generate test suites. There are a number of similar tools such as \textit{Randoop}~\cite{pacheco2007randoop} and \textit{Pex}~\cite{tillmann2008pex} (for .NET). 

Test generation tools aim for high coverage. In a study on the \textit{XCorpus}~\cite{xcorpus}, a subset of 70 Java programs from the \textit{Qualitas Corpus v 20130901}, we observed an average branch coverage of 55.86\%. This compares very favourably with the coverage of only 34.42\% obtained with build in tests for the 28 / 70 programs that had tests. The coverage of \textit{DaCapo} using the standard driver is also low (16.10\% average). One weakness of test generation tools is their inability to generate assertions representing intended program state, this is known as the \textit{oracle problem}~\cite{miller1981tutorial}. However, this is irrelevant for our purpose as we only use these tests to exercise the program, not to check state.  

The question whether generated tests can generate soundness oracles with interesting program behaviour is difficult to answer. In general, we do not expect generated tests to have any bias for or against language features in the program. As far as effort is concerned, test case generation is inexpensive as it can be completely automated. However, it consumes significant computational resources. Our experience with \textit{Evosuite} indicates that sometimes days are needed to compute a test suite with sufficient coverage for a medium sized library on standard commodity hardware~\cite{xcorpus}. 


\subsection{Stacktrace Mining}

Software repositories like GitHub and Bitbucket and forums like StackOverflow are rich sources of information. In particular, many users report problems by posting questions with included stack traces. Since these stack traces have a canonical format, they can be gathered and parsed. Many of the languages features that cause unsoundness use particular exception or error types to report problems, including (package names omitted for brevity): 

\begin{enumerate}
	\item \texttt{InstantiationException} -- reflective instantiation via \texttt{Class.newInstance()}
	\item \texttt{InvocationTargetException} 	-- reflective method calls via \texttt{Method.invoke(..) }
	\item \texttt{UndeclaredThrowableException} -- reflective method calls via \texttt{Invocation\-Handler.\-invoke(..)} used for proxies
	\item \texttt{javax.script.ScriptException}	-- may include a (reflective) call to a Java method when a script is evaluated
	
\end{enumerate}


When users encounter and report these exceptions, the stacktraces often reveal call graph edges associated with the use of dynamic language features. For instance, the following stack trace was reported on StackOverflow (\url{https://goo.gl/bfFRhG}):
 
\begin{lstlisting}[frame=single]                                                                       
Processing main file... 
Unable to start FOP: java.lang.reflect.InvocationTargetException 
	at .. 
	at java.lang.reflect.Method.invoke(Unknown Source) 
	at org.apache.fop.cli.Main.startFOPWithDynamicClasspath(Main.java:133) 
	at org.apache.fop.cli.Main.main(Main.java:207) 
Caused by: java.lang.NoClassDefFoundError: org/apache/xmlgraphics/image/loader/ImageContext 
	at ..	
	at org.apache.fop.cli.Main.startFOP(Main.java:157) 
	... 6 more 
Caused by: ...
\end{lstlisting}

This reveals a reflective method call of \texttt{org...Main.\-start\-FOP} using a call site in \texttt{org...Main.start\-FOP\-With\-Dynamic\-Class\-path()}. 

Th extracting of stacktraces from online sources can be automated to some extent. A challenge is the extraction of correct versioning information. 

\subsection{Common Vulnerabilities and Exposures (CVE)}

There are numerous vulnerabilities in the common vulnerabilities and exposures database (CVE) that exploit dynamic language features. An example is CVE-2015-7450~\footnote{\url{https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-7450} [accessed 15 March 17]}, a vulnerability in the Apache Commons Collections library that can be exploited to trigger remote code execution on certain IBM products. The attack exploits the fact that Java deserialisation of data structures triggers the execution of \textit{``trampoline''} methods, for instance, to compute the hash codes for elements of hashed containers. The vulnerabilities enable attackers to create call chains (\textit{``gadgets''}) connecting the trampoline methods with unsafe code, in this case \texttt{java.\-lang.\-Run\-time.exec}. The respective reflective call site is in \texttt{org.\-apache.\-commons.\-coll\-ections.\-functors.\-Invoker\-Trans\-former\#transform}. 

There are numerous known vulnerabilities following the same pattern, including CVE-2016-2510 (BeanShell) and CVE-2015-3253 (Groovy). The extraction of call graph edges from these vulnerabilities is expensive as it has to be performed manually, however, the results are usually of high value as they can be used to test static analysis tools that aim at detecting vulnerabilities. 

In addition to the official CVE registry, there are numerous other security-related discussion forums and web sites that are valuable sources of ``exotic behaviour'', such as Chris Frohoff's \textit{ysoserial} repository~\cite{ysoserial}. This contains call chains for numerous serialization-related vulnerabilities, including the call chains using reflection from CVE-2015-7450. There is also an example of how embedded compilation and class loading can be used to construct call graph edges and chains, respectively. 
In the \textit{Spring1} call chain~\footnote{\url{https://goo.gl/JFcHK2}, [accessed 17 March 17]}, the embedded XSLT compiler (\texttt{org.apache.xalan.xsltc.\-trax.\-Templates\-Impl}) is used to generate and load a class defined from data read from an incoming stream. The initialisation of this class then triggers the static block (\texttt{<clinit>}) to execute, which then invokes \texttt{Runtime.exec()}.

\subsection{Summary}

Table \ref{tab:summary} provides an overview of the methods discussed.

\begin{table}[h!]
	\centering
	\caption{Methods to generate soundness oracles}
	\label{tab:summary}
	\begin{tabular}{|llll|}
		\hline
		method					&		quality				& 	 size 			&	effort   \\ \hline
		on-board executables  	& 		high				& 	medium			& 	medium			\\
		generated tests			& 		medium				& 	large			& 	low				\\
		stacktrace mining		& 		high				& 	low				& 	medium			\\
		cve analysis			& 		high				& 	low				& 	high			\\	\hline			            
	\end{tabular}
\end{table}

\section{Conclusion}

In this paper, we have discussed Java language features that cause static analysis tools to make unsound choices. We then discussed four different approaches to build soundness oracles by means of dynamic analysis and mining data sources. 

The next step is to create those oracles, and to assess the output of static analysis tools against them.

\bibliographystyle{plain}
\bibliography{references}

\end{document}


