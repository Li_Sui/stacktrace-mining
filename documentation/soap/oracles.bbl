\begin{thebibliography}{10}

\bibitem{jsr199}
{JSR 199: Java Compiler API}, 2006.
\newblock \url{https://jcp.org/en/jsr/detail?id=199} [accessed 12 March 17].

\bibitem{jsr223}
{JSR 223: Scripting for the Java Platform}, 2006.
\newblock \url{https://jcp.org/en/jsr/detail?id=223} [accessed 12 March 17].

\bibitem{beck2003test}
Kent Beck.
\newblock {\em Test-driven development: by example}.
\newblock Addison-Wesley Professional, 2003.

\bibitem{blackburn2006dacapo}
Stephen~M Blackburn, Robin Garner, Chris Hoffmann, Asjad~M Khang, Kathryn~S
  McKinley, Rotem Bentzur, Amer Diwan, Daniel Feinberg, Daniel Frampton,
  Samuel~Z Guyer, et~al.
\newblock The dacapo benchmarks: Java benchmarking development and analysis.
\newblock In {\em Proceedings OOPSLA'06}. ACM, 2006.

\bibitem{bodden2012invokedynamic}
Eric Bodden.
\newblock Invokedynamic support in soot.
\newblock In {\em Proceedings SOAP'12}. ACM, 2012.

\bibitem{bodden2011taming}
Eric Bodden, Andreas Sewe, Jan Sinschek, Hela Oueslati, and Mira Mezini.
\newblock Taming reflection: Aiding static analysis in the presence of
  reflection and custom class loaders.
\newblock In {\em Proceedings ICSE'11}. ACM, 2011.

\bibitem{bravenboer2009strictly}
Martin Bravenboer and Yannis Smaragdakis.
\newblock Strictly declarative specification of sophisticated points-to
  analyses.
\newblock In {\em Proceedings OOPSLA'09}. ACM, 2009.

\bibitem{xcorpus}
Jens Dietrich, Henrik Schole, Li~Sui, and Ewan Tempero.
\newblock {XCorpus – An executable Corpus of Java Programs}, 2017.
\newblock \url{https://goo.gl/kGk7x3} [preprint, accessed 16 March 17].

\bibitem{ernst2003static}
Michael~D Ernst.
\newblock Static and dynamic analysis: Synergy and duality.
\newblock In {\em Proceedings WODA'03}, 2003.

\bibitem{fowler2004inversion}
Martin Fowler.
\newblock Inversion of control containers and the dependency injection pattern,
  2004.
\newblock \url{https://martinfowler.com/articles/injection.html} [accessed 12
  March 17].

\bibitem{fowler2010domain}
Martin Fowler.
\newblock {\em Domain-specific languages}.
\newblock Pearson Education, 2010.

\bibitem{fraser2011evosuite}
Gordon Fraser and Andrea Arcuri.
\newblock Evosuite: automatic test suite generation for object-oriented
  software.
\newblock In {\em Proceedings FSE'11}. ACM, 2011.

\bibitem{ysoserial}
Chris Frohoff.
\newblock {ysoserial -- A proof-of-concept tool for generating payloads that
  exploit unsafe Java object deserialization}, 2016.
\newblock \url{https://github.com/frohoff/ysoserial} [accessed 16 March 17].

\bibitem{jezek2016magic}
Kamil Jezek and Jens Dietrich.
\newblock Magic with dynamo--flexible cross-component linking for java with
  invokedynamic.
\newblock In {\em Proceedings ECOOP'16}. Schloss Dagstuhl-Leibniz-Zentrum fuer
  Informatik, 2016.

\bibitem{kafka2015metamorphosis}
Franz Kafka.
\newblock {\em The metamorphosis}.
\newblock WW Norton \& Company, 2015.

\bibitem{kiczales1997aspect}
Gregor Kiczales, John Lamping, Anurag Mendhekar, Chris Maeda, Cristina Lopes,
  Jean-Marc Loingtier, and John Irwin.
\newblock Aspect-oriented programming.
\newblock In {\em Proceedings ECOOP'97}. Springer, 1997.

\bibitem{landman2017challenges}
Davy Landman, Alexander Serebrenik, and Jurgen Vinju.
\newblock Challenges for static analysis of java reflection -- literature
  review and empirical study.
\newblock In {\em Proceedings ICSE'17}. ACM, 2017.

\bibitem{livshits2015defense}
Benjamin Livshits, Manu Sridharan, Yannis Smaragdakis, Ondrej Lhot{\'a}k,
  Jos{\'e}~Nelson Amaral, Bor-Yuh~Evan Chang, Samuel~Z Guyer, Uday~P Khedker,
  Anders M{\o}ller, and Dimitrios Vardoulakis.
\newblock In defense of soundiness: a manifesto.
\newblock {\em Commun. ACM}, 58(2):44--46, 2015.

\bibitem{Livshits2005Reflection}
Benjamin Livshits, John Whaley, and Monica~S. Lam.
\newblock Reflection analysis for java.
\newblock In {\em Proceedings APLAS'05}. Springer, 2005.

\bibitem{mastrangelo2015use}
Luis Mastrangelo, Luca Ponzanelli, Andrea Mocci, Michele Lanza, Matthias
  Hauswirth, and Nathaniel Nystrom.
\newblock Use at your own risk: the java unsafe api in the wild.
\newblock In {\em Proceedings OOPSLA'15}. ACM, 2015.

\bibitem{miller1981tutorial}
Edward Miller and William~E Howden.
\newblock {\em Tutorial: software testing \& validation techniques}.
\newblock IEEE Computer Society Press, 1981.

\bibitem{pacheco2007randoop}
Carlos Pacheco and Michael~D Ernst.
\newblock Randoop: feedback-directed random testing for java.
\newblock In {\em Proceedings OOPSLA'07}. ACM, 2007.

\bibitem{shivers1991control}
Olin Shivers.
\newblock {\em Control-flow analysis of higher-order languages}.
\newblock PhD thesis, Carnegie Mellon, 1991.

\bibitem{smaragdakis2015more}
Yannis Smaragdakis, George Balatsouras, George Kastrinis, and Martin
  Bravenboer.
\newblock More sound static handling of java reflection.
\newblock In {\em Proceedings APLAS'15}. Springer, 2015.

\bibitem{sridharan2011f4f}
Manu Sridharan, Shay Artzi, Marco Pistoia, Salvatore Guarnieri, Omer Tripp, and
  Ryan Berg.
\newblock F4f: taint analysis of framework-based web applications.
\newblock In {\em Proceedings OOPSLA'11}. ACM, 2011.

\bibitem{tillmann2008pex}
Nikolai Tillmann and Jonathan De~Halleux.
\newblock Pex--white box test generation for .net.
\newblock In {\em Proceedings TAP'08}. Springer, 2008.

\bibitem{vallee1999soot}
Raja Vall{\'e}e-Rai, Phong Co, Etienne Gagnon, Laurie Hendren, Patrick Lam, and
  Vijay Sundaresan.
\newblock Soot-a java bytecode optimization framework.
\newblock In {\em Proceedings CASCON'99}. IBM, 1999.

\end{thebibliography}
