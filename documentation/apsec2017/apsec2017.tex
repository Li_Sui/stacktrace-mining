
\documentclass[10pt, conference]{IEEEtran}

\usepackage{todonotes}
\usepackage{listings}
\usepackage{url}
\usepackage[bookmarks=false]{hyperref}
\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\ttfamily\scriptsize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{black},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{black},       % keyword style
	language=Octave,                 % the language of the code
	otherkeywords={*,...},           % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{black}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{black},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}


\begin{document}


\title{On the Use of Mined Stack Traces to Improve the Soundness of Statically Constructed Call Graphs}


% author names and affiliations
% use a multiple column layout for up to two different
% affiliations
% \author{Authors TBA}
% disabled for blind review
\author{\IEEEauthorblockN{Li Sui, Jens Dietrich, Amjed Tahir}
\IEEEauthorblockA{School of Engineering and Advanced Technology, Massey University, Palmerston North, New Zealand\\
Email: \{L.Sui,J.B.Dietrich,A.Tahir\}@massey.ac.nz}
}

\maketitle

\begin{abstract}

Static program analysis is a cornerstone of modern software engineering -- it is used to detect bugs and security vulnerabilities early before software is deployed. While there is a large body of research into the scalability and the precision of static analysis, the (un)soundness of static analysis is a critical issue that has not attracted the same level of attention by the research community. 

In this paper we investigate the question whether information harvested from stack traces obtained from the GitHub issue tracker and Stack Overflow Q\&A forums can be used in order to complement statically built call graphs. For this purpose, we extract reflective call graph edges from parsed stack traces, and check whether these edges are correctly computed by \textit{Doop}, a widely used tool for static analysis with built-in support for reflection analysis. We do find edges that \textit{Doop} misses when analysing real-world programs, even when reflection analysis is enabled. This suggests that mining techniques are a useful tool to test and improve the soundness of static analysis. 

\end{abstract}

\begin{IEEEkeywords}
stack trace analysis, static analysis, empirical studies, soundness, call graph construction,
mining software repositories
\end{IEEEkeywords}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle


\section{Introduction}

Static program analysis is an approach to optimise code and to detect vulnerabilities and bugs early in the lifecycle of a program. An example of such an analysis is taint analysis~\cite{newsome2005dynamic}. The objective here is to establish whether the execution of code starting at dedicated program entry points (such as Java's \texttt{main} methods) can execute code that can invoke methods such as \texttt{Runtime.exec()} with unsafe (tainted) parameter values. In other terms, static analysis is used to examine programs' proneness to code injection attacks.
 
Static analysis requires to build models of suitable program abstractions, in this case,  call graphs. Call graphs model the invocation of methods from call sites within other methods. Building a call graph by means of static analysis seems to be simple at first as method bodies contain references to the methods they invoke, but there are several complications. The first problem is \textit{precision}, caused by the fact that virtual method calls must be resolved (``devirtualised'') to model dynamic dispatch. There are several established methods to deal with this such as Class Hierarchy Analysis (CHA)  \cite{dean1995optimization}, taking into account method names, signatures and class hierarchy information. But this only yields a rather coarse over-approximation as it produces a large number of false positives: call graph edges that do not correspond to \textit{possible} method invocations. A more precise method is to use the actual types of the objects referenced at the call site, computed by a simultaneously performed points-to analysis~\cite{bacon1996fast,tip2000scalable}. While this so-called call-graph-construction on-the-fly generally yields better precision, it comes at a price: the algorithms used are complex, usually in $O(n^3)$ (dubbed the ``cubic bottle neck'' of program analysis \cite{heintze1997cubic}), as graph reachability problems (plain transitive closure or CFL reachability) must be solved, and even the best known algorithms are still super-quadratic~\cite{coppersmith1990matrix,chaudhuri2008subcubic}.

While progress has been made to improve the precision and scalability of static analysis, this has brought another issue to the fore. Static analysis is sometimes portrait as sound (as the entire program is analysed) but not precise. On the other hand, dynamic analysis that ``exercises'' the program under analysis is unsound (as it misses the parts of a program not exercised by a harness or driver) but precise (as it only reports actual behaviour) \cite{ernst2003static}. However, modern programming languages are full of dynamic features that are difficult to capture by static analysis, such as \textit{reflection}, \textit{proxies}, \textit{dynamic class loading} etc. Not capturing those features in a static analysis leads to under-approximation. This implies that the models analysed do not represent the entire program and the analysis is therefore also unsound as it produces false negatives. In applications like taint analysis, this can have serious consequences as the analysis would miss some vulnerabilities.  The need for more research into the soundness of static analysis has been recently highlighted by Livshits et al. \cite{livshits2015defense}. 

To illustrate this, consider the program in listing \ref{java:example}. This is a simple Java program that uses reflection. The reflective call site is in \texttt{b()}, the invoked method is \texttt{c()}. A sound static analysis should build the following call graph: \texttt{main(String[])} $\rightarrow$ \texttt{a()} $\rightarrow$ \texttt{b()} $\rightarrow$ \texttt{c()} $\rightarrow$ \texttt{d()}\footnote{There is an additional edge from \texttt{main(String[])} to the constructor \texttt{Foo()} which we ignore as it is not relevant to our discussion.}. In this work, the issue we are interested in is that static analysis tools will have problems computing the \texttt{b()} $\rightarrow$ \texttt{c()} edge.

Many static analysis tools will try to infer this by trying to interpret type references in cast statements or string literals that occur in the method, class, program or even configuration files (such as \texttt{web.xml} in J2EE applications) as class and method name. For instance, in listing \ref{java:example} the string literals in lines 8 and 9 can be directly interpreted as class and method names, respectively. But in general this is not that easy, as the references to the respective names and reflective objects must be tracked~\cite{Livshits2005Reflection}. But even this approach does not guarantee soundness if more complex programming patterns are used, e.g. if method names are passed as parameters, computed from conventions, if non-standard file formats are used to configure service class names or if methods are overloaded.

\lstset{language=Java,caption={A simple Java program with a reflective call},label=java:example}
\begin{lstlisting}
import java.lang.reflect.Method;
public class Foo {
  public static void main(String[] p) throws Exception{
    new Foo().a();
  }
  void a() throws Exception {b();}
  void b() throws Exception {
    Class c = Class.forName("Foo");
    Method m = c.getDeclaredMethod("c",new Class[]{});
    m.invoke(this, new Object[]{});
  }
  void c() { d();}
  void d() { throw new RuntimeException();}
}
\end{lstlisting}


In this paper, we evaluate whether data from public software repositories such as GitHub \footnote{https://github.com}, and Q\&A sites such as Stack Overflow \footnote{https://stackoverflow.com} can be used in order to augment unsound statically constructed call graphs. The idea is that there are many users that will have exercised (executed) a program, and the stack traces represent partial actual program behaviour. This behaviour might even be particularly interesting and valuable in the sense that it has led to exceptions, and therefore is likely to be a behaviour that has not been encountered during a standard dynamic analysis procedure such as testing. 

Consider again the program in listing \ref{java:example}. In line 13, a runtime exception is thrown. When a user encounters this exception, the stack trace shown in listing \ref{java:stacktrace} is generated. 

\lstset{language=Java,caption={Stacktrace for the Java program in listing \ref{java:example}, \$ is short for \texttt{java.lang.reflect}},label=java:stacktrace}
\begin{lstlisting}
Exception in thread "main" $.InvocationTargetException
  ..
  at $.Method.invoke(Method.java:498)
  at Foo.b(Foo.java:7)
  at Foo.a(Foo.java:6)
  at Foo.main(Foo.java:3)
  ..
Caused by: java.lang.RuntimeException
  at Foo.d(Foo.java:13)
  at Foo.c(Foo.java:12)
  ... 12 more
\end{lstlisting}
 
When an exception is thrown in a method invoked through reflection, an \texttt{InvocationTargetException} exception is generated that wraps the original exception using Java's standard exception chaining mechanism. The respective stack traces show this as two different blocks, with the method that has the reflective call site in the first block just below the reflective call site (\texttt{Method.invoke}), and the target at the bottom of the second block. This information can be used to infer the edge \texttt{b()} $\rightarrow$ \texttt{c()} and then add it to a statically constructed call graph. 
We discuss our data collection and analysis methodology in the following Section.

\section{Methodology}

We conducted experiments on the \textit{DaCapo 2009} data set~\cite{blackburn2006dacapo}, a Java benchmark that is widely used in programming language research. We augmented this dataset with several additional programs that are known to use reflection, including \textit{log4j-2.1},  \textit{antlr4-4.0}, \textit{hbase-hbase-client-0.98.0-hadoop1}, \textit{guava-11.0}, \textit{spring-boot-loader-1.2.5} and \textit{weld-core-impl-2.2.12} (by \textit{jboss}).

We investigated the use of APIs and replicated datasets to mine repository data, in particular the GitHub API \footnote{\url{https://api.github.com/} (accessed 17 January 2017)} and the GHTorent project~\cite{gousios2013ghtorent} that collects and publishes GitHub snapshots. Both had limitations that have rendered them as unsuitable for our study. The GitHub API does not support fine-grained text search, a limiting factor as we were interested to analyse pages from the issue tracking system containing certain strings. GHTorent does not store the bodies of the actual issues, but only the respective metadata. 

We therefore decided to  write a custom HTML client to search within GitHub issue tracker and Stack Overflow Q\&A sites for issues and discussions that include the text \texttt{java.lang.reflect.InvocationTargetException}
%search string: \texttt{is:issue java.lang.reflect.InvocationTargetException} 
%URL: \url{https://github.com/search?utf8=%E2%9C%93&q=is%3Aissue+java.lang.reflect.InvocationTargetException}

The client mimics a web browser session using the appropriate headers, and returns the URLs of the respective static issue web pages. We then downloaded the respective web pages, stripped HTML markup and extracted stack traces, instantiating the meta-model depicted in Figure \ref{fig:metamodel}. We also applied a filter to remove stack traces not containing any of the classes from our dataset. We extracted the respective class list using bytecode analysis on the respective programs. Then we inferred edges representing reflective method invocations from stack traces containing ``caused by'' clauses, as discussed above.  The web scraps were performed on 19 January 2017.

We also built static call graphs using \textit{Doop}~\cite{bravenboer2009strictly} with and without reflection analysis enabled\footnote{We have used the following \textit{Doop} options to enable reflection analysis: \texttt{--reflection --reflection-classic --reflection-high-soundness-mode --reflection-substring-analysis --reflection-invent-unknown-objects --reflection-refined-objects\\ --reflection-speculative-use-based-analysis}}, and checked whether any of the edges extracted were missing from these call graphs. The complete process is depicted in Figure \ref{fig:process}. At the end of this process, the first and second author manually validated the results by investigating the respective source code. To validate the correctness of our approach, we also conducted a manual cross-validation of the first 100 recorded stack traces which found 17 false positives results. Those stack traces were then eliminated.
%We also used \textit{Tamiflex},  a state-of-the-art tool to detect reflection that is often used with \textit{Soot}, although  \textit{Tamiflex} is technically not a static analysis tool as it instruments and then exercises the program under analysis.


\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{metamodel.pdf}
	\end{center}
	\caption{Stacktrace metamodel}
	\label{fig:metamodel}	
\end{figure}


\begin{figure}
	\begin{center}
		\includegraphics[width=8cm]{process.pdf}
	\end{center}
	\caption{Process to extract and analyse stacktrace data}
	\label{fig:process}	
\end{figure}


\section{Results}

\subsection{Overview}

We found 18,431 pages with references to \texttt{java...InvocationTargetException}, 11,932 from GitHub and 6,499 from Stack Overflow, from which we extracted 12,329 stack traces. From these stack traces we computed 11,920 reflective call graph edges. There are many cases of duplication, caused by stack traces reported on different pages describing the same call graph edge. After removing those duplicates, we ended up with 4,747 edges. Amongst those, 495 have a matching reflective call sites in our data set. To enable the comparative analysis with \textit{Doop}, we further removed edges with targets outside the data set. %This is the case if the reflective call is in an additional client-specific library or program. 
We found 15 unique edges that we could cross-reference with the statically built call graphs. Not surprisingly, the number of relevant edges we found was low as the respective discussions cover a wide range of different projects, most of them not in our data set. 

% A common pattern we have encountered were reflective calls from frameworks like spring into project specific code. 
% Note that we were unable to extract the version of the respective program for which the edges were extracted. But we manually verified that the respective invocations exist in the versions of the programs we analysed with \textit{Doop}

We discuss some of the results in more detail in the following subsections. Out of the 15 results, we discuss four edges in detail and provide the URLs for both the page from which we extracted the stack trace, and the source code of the analysed version of the class with the reflective call site~\footnote{For space reasons, we have used a URL shortener.}. 

%The two other edges found link the \textit{DaCapo} driver and the \texttt{Eclipse} / \texttt{Batik} runner respectively, and are less relevant as they are not part of a real-world program, but only of an utility used to experiment with such programs.
Static analysis with \textit{Doop} created two call graphs depending on whether reflection analysis was enabled or not. Without reflection analysis, \textit{Doop} does not find any of the edges we recovered from stack traces. With reflection analysis enabled, \textit{Doop} finds only 4 of the 15 edges. We discuss some of the edges found in more detail next.

\subsection{Fop in DaCapo-9.12}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:fop-0.95}
	\begin{tabular}{rp{4cm}}
		% project: 	&  fop (DaCapo-9.12)\\
		call site: 	&  \footnotesize{\texttt{org.apache.fop.cli.Main\# startFOPWithDynamicClasspath}} \\
		target:		&  \texttt{org.apache.fop.cli.Main\#startFOP} \\
		stacktrace: &  \url{https://goo.gl/bfFRhG} \\
		source:		&  \url{https://goo.gl/JoXoam}
	\end{tabular}
\end{table}

\textit{Fop} is part of \textit{DaCapo}. The \texttt{Main} class contains some reflective code that contains literals for the class and the method name of the invocation target, and the parameter types. Advanced static analysis tools should be able to correctly extract this edge, and this is indeed the case for \textit{Doop}: if reflection analysis is enabled, \textit{Doop} will find this edge. 

 \lstset{language=Java,caption={Reflective method invocation in fop, from \texttt{\url{https://goo.gl/JoXoam}}, lines 140--143},label={java:fop}}
 \begin{lstlisting}
Class clazz = Class.forName("org.apache.fop.cli.Main",true,loader);
Method mainMethod = clazz.getMethod("startFOP",new Class[]{String[].class});
mainMethod.invoke(null,new Object[]{args});
 \end{lstlisting}
 
\subsection{Antlr4-4.0}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:antlr}
	\begin{tabular}{rp{4cm}} 
		% project: 	&  antlr-4.6\\
		call site: 	&  \texttt{org.antlr.v4.parse.GrammarTreeVisitor\#visit} \\
		target:		&  \texttt{org.antlr.v4.parse.GrammarTreeVisitor\# grammarSpec} \\
		stacktrace: &  \url{https://goo.gl/B0ZAOb} \\
		source:		&  \url{https://goo.gl/g6JYdN}
	\end{tabular}
\end{table}

\textit{Antlr} is a popular parser generator. The reflective call we detected is similar to the invocation described above in \textit{Fop}, although slightly more sophisticated: in \texttt{GrammarTreeVisitor}, the \texttt{visitGrammar(GrammarAST)} method invokes \texttt{visit(GrammarAST,String)} using the string literal \texttt{"grammarSpec"} as a second \texttt{ruleName} parameter. The rule name is then interpreted as method name in \texttt{visit(GrammarAST,String)}. Advanced static analysis tools that track string literals interpreted as method names across procedures would still be able to find this edge. Indeed, \textit{Doop} with reflection analysis enabled will find this edge. 

\subsection{Hbase-client-0.98.0-hadoop1}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:hadoop}
	\begin{tabular}{rp{4cm}}
		% project: 	&  hadoop-hbase-client-0.98.8\\
		call site: 	&  \footnotesize{\texttt{org.apache.hadoop.hbase.protobuf.Protobuf\-Util\#toFilter }} \\
		target:		&  \texttt{org.apache.hadoop.hbase.filter.FilterList\# parseFrom} \\
		stacktrace: &  \url{https://goo.gl/RUZ027} \\
		source:		&  \url{https://goo.gl/bX3ffV}
	\end{tabular}
\end{table}

\textit{Hadoop} is a popular framework for storing and processing big data. The reflective call detected in hadoop invokes a method with a fixed name (\texttt{"parseFrom"}) and signature (\texttt{byte[].class}), both are defined within this method. But the class that provides the method is computed using a dynamic class loader that is configured with information read from project-specific configuration files. General-purpose static analysis tools are unlikely to precisely model this. However, the class must extend \texttt{org.apache.hadoop.hbase.filter.Filter} , and a possible approach is to ensure soundness by over-approximating the analysis. This can be achieved by adding edges to all \texttt{"parseFrom(byte[])"} methods implemented in subclasses of \texttt{Filter}. This means that soundness can be achieved by compromising precision. \textit{Doop} with reflection analysis enabled will find this edge. 


\subsection{Log4J-2.1}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:log4j}
	\begin{tabular}{rp{4cm}}
		% project: 	&  log4j-2.7\\
		call site: 	&  \footnotesize{\texttt{org.apache.logging.log4j.core.config.\-plugins.util.PluginBuilder\#build}} \\
		target:		&  \texttt{org.apache.logging.log4j.core.appender.\-RollingFileAppender\#createAppender} \\
		stacktrace: &  \url{https://goo.gl/Ohg7lo} \\
		source:		&  \url{https://goo.gl/o885Hw}
	\end{tabular}
\end{table}

\textit{Log4j} is a widely used logging framework. The use of reflection that creates the reflective call site is the most sophisticated we have encountered, however, this is common for frameworks that supports plugins. Reflection is used to achieve loose coupling and sandboxing. In the  \texttt{PluginBuilder\#build} method, the detection of the method invoked is delegated to \texttt{findFactoryMethod}. This method detects the \textit{first static} method it can find annotated with \texttt{@PluginFactory}. This semantics is almost impossible to capture with static analysis tools, and the only strategy possible here would be to over-approximate. \textit{Doop}, even with reflection analysis enabled, will not find this edge. 


\section{Threads to Validity}


There are a few potential issues with the extraction process. We might have missed some stack traces that are formatted in unusual ways - for instance, stack traces produced by log frameworks that allow custom stack trace formatting. This would have given us some more results, but based on our own experience we think that this did not have a major impact.  
% we noticed the lack of versioning information in many web sites, or the difficulty to extract this information when it was present. This means that we might have extracted edges not present in the version of the program we statically analysed.

There are several issues that could have caused false positives. Firstly, stack traces only contain methods names, but neither signatures nor descriptors. This can introduce false positives when methods are overloaded. Secondly, imprecise parsing could have produced false positives. We sampled 100 results to validate the correctness of parsed stack traces, 17 false positives were found. Thirdly, stack traces lack version information, although in some cases version information can be found on the enclosing web sites. This means that we might have extracted edges not present in the version of the program we statically analysed. We have mitigated this issue by running a script that matched the line numbers found in stack traces against program versions, and then selected the best matched version for future analysis.

We addressed all of the issues related to precision by manually checking those 15 edges obtained against the version of the source code of the program we analysed with \textit{Doop}. 

The scripts used to extract and process stack traces are available from the repository\footnote{\url{https://bitbucket.org/Li_Sui/program-analysis-spikes}}.


\section{Related Work}

\subsection{Reflection and Static Analysis}

Several authors have tried to improve the handling of reflection by static analysis. We only discuss the most influential work, for a recent in-depth discussion of works in this area readers are referred to the recent literature review by Landman et al~\cite{landman2017challenges}. Livshits et al \cite{Livshits2005Reflection} have investigated how to use points-to analysis to handle reflection. They associate objects with reflective call sites, and track strings that are interpreted as class names. Furthermore, they use information in casts often associated with reflective object creation sites to improve the precision of their analysis. They demonstrated that with this approach, a large percentage of \texttt{Class.forName} calls can be resolved. This approach relies on the precision of the underlying points-to analysis, and therefore users must make trade-offs between precision and scalability when applying this approach. Smaragdakis et al~\cite{smaragdakis2015more} have further refined this approach by adding substring and string flow analysis, further improving soundness.

Wala \cite{wala} is another static analysis tool developed by IBM. It employs a context-sensitivity policy to deal with reflections. Constructs like \texttt{Class.forName()}, \texttt{Class.newInstance()}, \texttt{Method.invoke()} are supported by this library

% Amjed:  new - added refere to the SOAP paper
In our recent work, we discussed some specific Java features that may cause \textit{unsoundness} of static analysis \cite{dietrich2017construction}.

\subsection{Hybrid Analysis}

One possible approach to tackle the unsoundness of static analysis is to combine it with a dynamic (pre-) analysis that informs the static analysis by recording actual program behaviour. 

Bodden et al \cite{bodden2011taming} proposed a tool called \textit{tamiflex} to improve the soundness of static analysis. \textit{Tamiflex} is a hybrid analysis tool that uses additional information inferred from recorded program runs to improve call graphs. The quality of these information relies on the fact that the driver (harness) used in order to exercise the program has a high coverage. The overall idea of \textit{tamiflex} is similar to our approach in that the mined stack traces can be considered as partial execution logs. \textit{tamiflex} is now widely used with the popular \textit{soot} analysis platform. Grech et al \cite{grechheaps} have extended this approach by also harvesting information from heap dumps that is then fed back into the static analysis performed by \textit{Doop} in the form of additional relations. 

\subsection{Stack Trace Analysis}

Several works have looked into mining and analysing large stack traces for the purpose of program comprehension and analysis. 
Kim et al~\cite{kim2011crash} proposed a technique called ``crash graph'' that uses stack traces from crash reports to predict fixable crashes and detecting duplicate crash reports. Sinha et al~\cite{sinha2009fault} provided a stack trace - based approach (based on static and dynamic analysis) to locate and fix bugs that are caused by runtime exceptions. Schr{\"o}ter et al. \cite{schroter2010stack} studied how stack traces can be helpful for developers to locating and fix bugs. The study found that developers tend to fix bugs faster when the stack traces are reported with an issue. 
Cabral and Marques \cite{cabral2007exception} analysed stack traces of 32 Java and .NET open-source projects.  They found that exceptions are not being correctly used by developers as error recovery mechanism. They observed that the actions inside handlers were often very simple (e.g. logging and presenting a message to the user). Coelho et al \cite{CAGDT16} studied Java stack traces from over 600 open source Android project. The goal of this study was to understand  bug hazards related to exception handling code. The study also surveyed their findings with 71 real App developers to understand how developers perceive the exception handling that leads to bug hazards (i.e. the potential of introducing bugs).
% Jens: shortened this a bit to create some space
% Amjed: now that we have space I though we can use this...  
%The study found that several unhandled exception can lead to bug hazards such as cross-type exception (e.g. \texttt{OutOfMemoryError}), undocumented runtime exceptions raised and undocumented checked exceptions signalled by native C code. The study also found that errors in programming logic expectation such as \texttt{NullPointerException} are responsible for the majority of bugs in the investigated programs.
 


\section{Conclusion}

In this paper, we have investigated the question whether information harvested from online data sources can be used to check and improve the soundness of static program models. The short answer is yes. Even with reflection analysis enabled, \textit{Doop} will find only 4 out of the 15 edges we extracted. While we have to be cautious to generalise this, it is obvious that there are many usages of dynamic language features that even the most advanced static analysis cannot capture. 

We argue that while our analysis does not provide a large number of call graph edges that can enhance the static analysis, it is useful to retrieve interesting (and in this sense, high-quality) edges that can point to the weaknesses of static analysis tools. This is hardly surprising: it seems more likely that a programmer encounters an exception or error if the software is used in a way that was not intended by the programmer, e.g. by bypassing program invariants or boundary checks. But those are exactly the cases of interest to static analysis as it has the ambition to discover those cases in order to reveal bugs and vulnerabilities.

One could argue that the use of hybrid analysis~\cite{bodden2011taming,grechheaps} addresses problems with (un-)soundness. The main challenge is to create drivers (harnesses) that exercise the ``unsound parts'' of a program. The use of test case generation / fuzzing techniques for this purpose is promising~\cite{xcorpus}. It seems that hybrid analysis techniques can mitigate, but not solve the problem. Extending this study by cross-referencing the call graphs with the call graphs produced by \textit{tamiflex} or similar tools is an interesting and relevant topic.

An extended study with a larger dataset is an interesting topic for future research. One particular interesting issue is the study of call graphs that cover multiple projects and libraries, including frameworks known for their heavy use of reflection (plugin-based systems, dependency injection) and the Java core libraries. We have noticed a large number of reflective invocation where call site and target were in different libraries. One potential problem here is that it is still challenging to build comprehensive and sufficiently precise static models for real-world programs that include all library dependencies, although new algorithms and tools are under development to address scalability issues~\cite{dietrich2015giga}.

% AMJED: this is just repeating whatever has been noted in this paper - I suggest to remove.
%A particular interesting fact is how different the edges detected are. As discussed above, some could be detected by tools implementing state of the art reflection analysis \cite{Livshits2005Reflection,bodden2011taming,smaragdakis2015more,grechheaps}, while for others this seems highly unlikely due to the level of sophistication of the use of reflection.  

%Building a dataset that contains a variety of edges and usage patterns which can be used as a benchmark for static analysis studies (and tools) is another interesting topic for future research.


\section*{Acknowledgement}

The authors would like to thank Georgios Gousios and Davy Landman for their comments. This work was funded by a New Zealand NSC-SEED Project (PROP-52515-NSCSEED-MAU).


\bibliographystyle{plain}
\bibliography{references}


% that's all folks
\end{document}


