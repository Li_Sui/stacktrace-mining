
\documentclass[10pt, conference]{IEEEtran}

\usepackage{todonotes}
\usepackage{listings}
\usepackage{url}
\usepackage{hyperref}
\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\ttfamily\scriptsize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{black},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{black},       % keyword style
	language=Octave,                 % the language of the code
	otherkeywords={*,...},           % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{black}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{black},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{On the Use of Mined Stack Traces to Improve the Soundness of Statically Constructed Call Graphs}
%Alternative title: Mining stack traces to imporve the soundess of statically constructed call graphs


% author names and affiliations
% use a multiple column layout for up to two different
% affiliations
\author{Authors TBA}
% disabled for blind review
%\author{\IEEEauthorblockN{Li Sui, Jens Dietrich, Amjed Tahir}
%\IEEEauthorblockA{School of Engineering and Advanced Technology, Massey University, Palmerston North, New Zealand\\
%Email: leesui0207@gmail.com,\{j.b.dietrich,a.tahir\}@massey.ac.nz}
%}

\maketitle

\begin{abstract}

Static program analysis is a cornerstone of modern software engineering -- it is used to detect bugs and security vulnerabilities early before software is deployed. While there is a large body of research into the scalability and the precision of static analysis, the (un)soundness of static analysis is a critical issue that has not attracted the same level of attention by the research community. 

In this paper we explore whether information harvested from stack traces obtained from GitHub issue tracker and Stack Overflow Q\&A forum can be used in order to complement statically built call graphs. For this purpose, we extract reflective call graph edges from parsed stack traces, and check whether these edges are correctly computed by \textit{Soot}, a widely used tool for static analysis. We find several edges that \textit{Soot} misses when analysing real-world programs. This suggests that mining techniques are a useful tool to test and improve the soundness of static analysis. 

\end{abstract}

\begin{IEEEkeywords}
stack trace analysis, static analysis, empirical studies, soundness, call graph construction,
mining software repositories
\end{IEEEkeywords}


% For peer review papers, you can put extra information on the cover
% page as needed:
% \ifCLASSOPTIONpeerreview
% \begin{center} \bfseries EDICS Category: 3-BBND \end{center}
% \fi
%
% For peerreview papers, this IEEEtran command inserts a page break and
% creates the second title. It will be ignored for other modes.
\IEEEpeerreviewmaketitle


\section{Introduction}

Static program analysis is an approach to optimise code and to detect vulnerabilities and bugs early in the lifecycle of a program. An example of such an analysis is taint analysis~\cite{newsome2005dynamic}. The objective here is to establish whether the execution of code starting at dedicated program entry points (such as Java's \texttt{main} method) can execute code that can invoke unsafe methods such as \texttt{Runtime.exec()}. In other terms, such an analysis is used to establish whether a program is prone to injection attacks or not.
 
Taint analysis requires to build models of suitable program abstractions, in this case,  call graphs. Call graphs model the invocation of methods from call sites within other methods. Building a call graph by means of static analysis seems to be simple at first as method bodies contain references to the methods they invoke, but there are several complications. The first problem is \textit{precision}, caused by the fact that virtual method calls must be resolved (``devirtualised'') to model dynamic dispatch. There are several established methods to deal with this such as class hierarchy analysis (CHA)  \cite{dean1995optimization}, taking into account method names, signatures and class hierarchy information. But this only yields a rather coarse over-approximation as it produces a large number of false positives: call graph edges that don't correspond to actual method invocations. A more precise method is to use the actual types of the objects referenced at the call site, computed by a simultaneously performed points-to analysis~\cite{bacon1996fast,tip2000scalable}. While this so-called call-graph-construction on-the-fly generally yields better precision, it comes at a price: the algorithms used are complex, usually in $O(n^3)$ (dubbed the ``cubic bottle neck'' of program analysis \cite{heintze1997cubic}), as graph reachability problems (plain transitive closure or CFL reachability) must be solved, and even the best known algorithms are still super-quadratic~\cite{coppersmith1990matrix,chaudhuri2008subcubic}.

While progress has been made to improve the precision and scalability of static analysis, this has brought another issue to the fore. The classic view is that static analysis is sound (as the entire program is analysed) but not precise. On the other hand, dynamic analysis that ``exercises'' the program under analysis is unsound (as it missed the parts of a program not exercised by a harness or driver) but precise (as it only reports actual behaviour) \cite{ernst2003static}. However, modern programming languages are full of dynamic features that are difficult to capture by static analysis, such as reflection, proxies, dynamic class loading etc. Not capturing those features in a static analysis leads to under-approximation. This implies that the models analysed do not represent the entire program and the analysis is therefore unsound as it produces false negatives. In applications like taint analysis, this can have serious consequences as the analysis would miss some vulnerabilities.  The need for more research into the soundness of static analysis has been recently highlighted \cite{livshits2015defense}. 

To illustrate this, consider the program in listing \ref{java:example}. This is a simple Java program that uses reflection. The reflective call site is in \texttt{b()}, the invoked method is \texttt{c()}. A sound static analysis should build the following call graph: \texttt{main(String[])} $\rightarrow$ \texttt{a()} $\rightarrow$ \texttt{b()} $\rightarrow$ \texttt{c()} $\rightarrow$ \texttt{d()}\footnote{There is an additional edge from \texttt{main(String[])} to the constructor \texttt{Foo()} which we ignore as it is not relevant for our discussion.}. In this work, the issue we are interested in is that static analysis tools will have problems computing the \texttt{b()} $\rightarrow$ \texttt{c()} edge.

Many static analysis tools will try to infer this by trying to interpret type references in cast statements or string literals that occur in the method, class, program or even configuration files (such as \texttt{web.xml} in J2EE applications) as class and method name. For instance, in listing \ref{java:example} the string literals in lines 8 and 9 can be directly interpreted as class and method names, respectively. But in general this is not that easy, as the references to the respective names and reflective objects must be tracked~\cite{Livshits2005Reflection}. But even this approach does not guarantee soundness if more complex programming patterns are used, e.g., if method names are passed as parameters, computed from conventions, if non-standard file formats are used to configure service class names or if methods are overloaded.

\lstset{language=Java,caption={A simple Java program with a reflective call},label=java:example}
\begin{lstlisting}
import java.lang.reflect.Method;
public class Foo {
  public static void main(String[] p) throws Exception{
    new Foo().a();
  }
  void a() throws Exception {b();}
  void b() throws Exception {
    Class c = Class.forName("Foo");
    Method m = c.getDeclaredMethod("c",new Class[]{});
    m.invoke(this, new Object[]{});
  }
  void c() { d();}
  void d() { throw new RuntimeException();}
}
\end{lstlisting}


In this research, we evaluate whether data from public repositories such as GitHub, and Q\&A sites such as Stack Overflow can be used in order to augment unsound statically constructed call graphs. The idea is that there are many users that will have exercised (executed) a program, and the stack traces represent partial actual program behaviour. This behaviour might even be particularly interesting and valuable in the sense that it has led to exceptions, and therefore is likely to be a behaviour that has not been encountered during a standard dynamic analysis procedure such as testing. 

Consider again the program in listing \ref{java:example}. In line 11, a runtime exception is thrown. When a user encounters this exception, the stack trace shown in listing \ref{java:stacktrace} is generated. 

\lstset{language=Java,caption={Stacktrace for the Java program in listing \ref{java:example}, \$ is short for \texttt{java.lang.reflect}},label=java:stacktrace}
\begin{lstlisting}
Exception in thread "main" $.InvocationTargetException
  ..
  at $.Method.invoke(Method.java:498)
  at Foo.b(Foo.java:7)
  at Foo.a(Foo.java:6)
  at Foo.main(Foo.java:3)
  ..
Caused by: java.lang.RuntimeException
  at Foo.d(Foo.java:13)
  at Foo.c(Foo.java:12)
  ... 12 more
\end{lstlisting}
 
When an exception is thrown in a method invoked through reflection, an \texttt{InvocationTargetException} exception is generated that wraps the original exception using Java's standard exception chaining mechanism. The respective stack traces show this as two different blocks, with the method that has the reflective call site in the first block just below the reflective call site (\texttt{Method.invoke}), and the target at the bottom of the second block. This information can be used to infer the edge \texttt{b()} $\rightarrow$ \texttt{c()} and then add it to a statically constructed call graph. 

\section{Methodology}

We conducted experiments on the \textit{DaCapo 2009} data set~\cite{blackburn2006dacapo}, a Java benchmark that is widely used in programming language research. We augmented this dataset with several additional programs that are known to use reflection, including \textit{log4j-2.7},  \textit{antlr-4.6} and \textit{hadoop-hbase-client-0.98.8}. 

We investigated the use of APIs and replicated datasets to mine repository data, in particular the GitHub API \footnote{\url{https://api.github.com/} (accessed 17 January 2017)} and the GHTorent project~\cite{gousios2013ghtorent} that collects and publishes GitHub snapshots. Both had limitations that have rendered them as unsuitable for our study. The GitHub API does not support fine-grained text search, we are interested to analyse pages from the issue tracking system containing the string \texttt{java.lang.reflect.InvocationTargetException}. GHTorent does not store the bodies of the actual issues, but only the respective metadata. 

We therefore decided to  write a custom HTML client to performed searches on the GitHub and Stack Overflow Q\&A web sites for issues and discussions including the text \texttt{java.lang.reflect.InvocationTargetException}
%search string: \texttt{is:issue java.lang.reflect.InvocationTargetException} 
%URL: \url{https://github.com/search?utf8=%E2%9C%93&q=is%3Aissue+java.lang.reflect.InvocationTargetException}

The client mimics a web browser session using the appropriate headers, and returns the URLs of the respective static issue web pages. We then downloaded the respective web pages, stripped HTML markup and extracted stack traces, instantiating the meta-model depicted in figure \ref{fig:metamodel}. We also applied a filter to remove stack traces not containing any of the classes from our dataset. We extracted the respective class list using bytecode analysis on the respective programs. Then we inferred edges representing reflective method invocations from stack traces containing ``caused by'' clauses, as discussed above.  The web scraps were performed on 19 January 2017.

We also built static call graphs using \textit{Soot}~\cite{vallee1999soot}, and checked whether any of the edges extracted were missing from these call graphs. The complete process is depicted in figure \ref{fig:process}. At the end of this process, we manually validated the results by investigating the respective source code. We also used \textit{Tamiflex},  a state-of-the-art tool to detect reflection that is often used with \textit{Soot}, although  \textit{Tamiflex} is technically not a static analysis tool as it instruments and then exercises the program under analysis.


\begin{figure}
	\includegraphics[width=9cm]{metamodel.pdf}
	\caption{Stacktrace metamodel}
	\label{fig:metamodel}	
\end{figure}

\begin{figure}
	\includegraphics[width=9cm]{process.pdf}
	\caption{Process to extract and analyse stacktrace data}
	\label{fig:process}	
\end{figure}

\section{Results}

\subsection{Overview}

We found 18,431 pages with references to \texttt{java.lang.reflect.InvocationTargetException}, 11,932 from GitHub and 6,499 from Stack Overflow, from which we extracted 12,329 stack traces. From these stack traces we computed 11,920 reflective call graph edges. There are many cases of duplication, caused by stack traces reported on different pages describing the same call graph edge. After removing those duplicates, we ended up with 4,747 edges. Among them, we found six unique edges that we could cross-reference with the statically built call graphs. Not surprisingly, the number of relevant edges we found was low as the respective discussions cover a wide range of different projects, most of them not in our data set. 


% A common pattern we have encountered were reflective calls from frameworks like spring into project specific code. 

We discuss some of the results in more detail in the following subsections. Note that we were unable to extract the version of the respective program for which the edges were extracted. But we manually verified that the respective invocations exist in the versions of the programs we analysed with \textit{Soot}. Out of the six results, we discuss four edges in detail and also provide the URLs for both the page from which we extracted the stack trace, and the source code of the analysed version of the class with the reflective call site~\footnote{For space reasons, we have used a URL shortener.}. The two other edges found link the \textit{DaCapo} driver and the \texttt{Eclipse} / \texttt{Batik} runner respectively, and are less relevant as they are not part of a real-world program, but only of an utility used to experiment with such programs.

Reflection analysis with \textit{Tamiflex} did not find any of the reflective edges we extracted from GitHub and Stack Overflow. 

\subsection{Fop in DaCapo-9.12}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:fop-0.95}
	\begin{tabular}{rp{4cm}}
		% project: 	&  fop (DaCapo-9.12)\\
		call site: 	&  \footnotesize{\texttt{org.apache.fop.cli.Main\# startFOPWithDynamicClasspath}} \\
		target:		&  \texttt{org.apache.fop.cli.Main\#startFOP} \\
		stacktrace: &  \url{https://goo.gl/bfFRhG} \\
		source:		&  \url{https://goo.gl/JoXoam}
	\end{tabular}
\end{table}

\textit{Fop} is part of \textit{DaCapo}. The \texttt{Main} class contains some reflective code that contains literals for the class and the method name of the invocation target, and the parameter types. Advanced static analysis tools should be able to correctly extract this edge. 

 \lstset{language=Java,caption={Reflective method invocation in fop, from \texttt{\url{https://goo.gl/JoXoam}}, lines 140--143},label={java:fop}}
 \begin{lstlisting}
Class clazz = Class.forName("org.apache.fop.cli.Main",true,loader);
Method mainMethod = clazz.getMethod("startFOP",new Class[]{String[].class});
mainMethod.invoke(null,new Object[]{args});
 \end{lstlisting}
 
\subsection{Antlr-4.6}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:antlr}
	\begin{tabular}{rp{4cm}} 
		% project: 	&  antlr-4.6\\
		call site: 	&  \texttt{org.antlr.v4.parse.GrammarTreeVisitor\#visit} \\
		target:		&  \texttt{org.antlr.v4.parse.GrammarTreeVisitor\# grammarSpec} \\
		stacktrace: &  \url{https://goo.gl/B0ZAOb} \\
		source:		&  \url{https://goo.gl/g6JYdN}
	\end{tabular}
\end{table}

\textit{Antlr} is a popular parser generator. The reflective call we detected is similar to the invocation described above in \textit{Fop}, although slightly more sophisticated: in \texttt{GrammarTreeVisitor}, the \texttt{visitGrammar(GrammarAST)} method invokes \texttt{visit(GrammarAST,String)} using the string literal \texttt{"grammarSpec"} as a second \texttt{ruleName} parameter. The rule name is then interpreted as method name in \texttt{visit(GrammarAST,String)}. Advanced static analysis tools that track string literals interpreted as method names across procedures would still be able to find this edge. 

\subsection{Hadoop-hbase-client-0.98.8}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:hadoop}
	\begin{tabular}{rp{4cm}}
		% project: 	&  hadoop-hbase-client-0.98.8\\
		call site: 	&  \footnotesize{\texttt{org.apache.hadoop.hbase.protobuf.Protobuf\-Util\#toFilter }} \\
		target:		&  \texttt{org.apache.hadoop.hbase.filter.FilterList\# parseFrom} \\
		stacktrace: &  \url{https://goo.gl/RUZ027} \\
		source:		&  \url{https://goo.gl/bX3ffV}
	\end{tabular}
\end{table}

\textit{Hadoop} is a popular framework for storing and processing big data. The reflective call detected in hadoop invokes a method with a fixed name (\texttt{"parseFrom"}) and signature (\texttt{byte[].class}), both are defined within this method. But the class that provides the method is computed using a dynamic class loader that is configured with information read from project-specific configuration files. General-purpose static analysis tools are unlikely to precisely model this. However, the class must extend \texttt{org.apache.hadoop.hbase.filter.Filter} , and a possible approach is to ensure soundness by over-approximating the analysis. This can be achieved by adding edges to all \texttt{"parseFrom(byte[])"} methods implemented in subclasses of \texttt{Filter}. This means that soundness can be achieved by compromising the precision.


\subsection{Log4J-2.7}

\begin{table}[h!]
	\footnotesize
	\label{tab:results:log4j}
	\begin{tabular}{rp{4cm}}
		% project: 	&  log4j-2.7\\
		call site: 	&  \footnotesize{\texttt{org.apache.logging.log4j.core.config.\-plugins.util.PluginBuilder\#build}} \\
		target:		&  \texttt{org.apache.logging.log4j.core.appender.\-RollingFileAppender\#createAppender} \\
		stacktrace: &  \url{https://goo.gl/Ohg7lo} \\
		source:		&  \url{https://goo.gl/o885Hw}
	\end{tabular}
\end{table}

\textit{Log4j} is a widely used logging framework. The use of reflection that creates the reflective call site is the most sophisticated we have encountered, however, this is common for frameworks that supports plugins. Reflection is used to achieve loose coupling and sandboxing. In the  \texttt{PluginBuilder\#build} method, the detection of the method invoked is delegated to \texttt{findFactoryMethod}. This method detects the \textit{first static} method it can find annotated with \texttt{@PluginFactory}. This semantics is almost impossible to capture with static analysis tools, and the only strategy possible here would be to over-approximate.


\section{Threads to Validity}


There are a few potential issues with the extraction process. We might have missed some stack traces that are formatted in unusual ways - for instance, stack traces produced by log frameworks that allow custom stack trace formatting. This would have given us some more results, but based on our own experience we think that this does not have a major impact.

There are several issues that could have caused false positives.  Firstly, we noticed the lack of versioning information in many web sites, or the difficulty to extract this information when it was present. This means that we might have extracted edges not present in the version of the program we statically analysed. Secondly, stack traces only contain methods names, but neither signatures nor descriptors. This can introduce false positives when methods are overloaded. Thirdly, imprecise parsing could have produced false positives.

We addressed all of the issues related to precision by manually checking the small number of results obtained against the version of the source code of the program we analysed with \textit{Soot}.

The data set and the scripts used to extract and process stack traces are available from the following public repository: [URL tba after review].


\section{Related Work}

\subsection{Reflection and Static Analysis}

Several authors have tried to improve the handling of reflection by static analysis. We only discuss the most influential work, for a recent in-depth discussion of works in this area readers are referred to the recent literature review by Landman et al~\cite{landman2017challenges}. Livshits et al \cite{Livshits2005Reflection} have investigated how to use points-to analysis to handle reflection. They associate objects with reflective call sites, and track strings that are interpreted as class names. Furthermore, they use information in casts often associated with reflective object creation sites to improve the precision of their analysis. They demonstrated that with this approach, a large percentage of \texttt{Class.forName} calls can be resolved. This approach relies on the precision of the underlying points-to analysis, and therefore users must make trade-offs between precision and scalability when applying this approach. Smaragdakis et al~\cite{smaragdakis2015more} have further refined this approach by adding substring and string flow analysis, further improving soundness.

Bodden et al \cite{bodden2011taming} proposed a tool called \textit{Tamiflex} to improve the soundness of static analysis. \textit{Tamiflex} is a hybrid analysis tool that uses additional information inferred from recorded program runs to improve call graphs. The quality of these information relies on the fact that the driver (harness) exercising the program has a high coverage. The overall idea of \textit{Tamiflex} is similar to our approach in that the mined stack traces can be considered as partial execution logs. \textit{Tamiflex} is now widely used with the popular \textit{Soot} analysis platform. 

\subsection{Stack Trace Analysis}

Several works have looked into mining and analysing large stack traces for the purpose of program comprehension and analysis. 
Kim et al~\cite{kim2011crash} proposed a technique called ``crash graph'' that uses stack traces from crash reports to predict fixable crashes and detecting duplicate crash reports. Sinha et al~\cite{sinha2009fault} provided a stack trace - based approach (based on static and dynamic analysis) to locate and fix bugs that are caused by runtime exceptions i). Schr{\"o}ter et al. \cite{schroter2010stack} studied how stack traces can be helpful for developers to locating and fix bugs. The study found that developers tend to fix bugs faster when the stack traces are reported with an issue. 
Cabral and Marques \cite{cabral2007exception} analysed stack traces of 32 Java and .NET open-source projects.  They found that exceptions are not being correctly used by developers as error recovery mechanism. They observed that the actions inside handlers were often very simple (e.g., logging and presenting a message to the user). Coelho et al \cite{CAGDT16} studied Java stack traces from over 600 open source Android project. The goal of this study was to understand  bug hazards related to exception handling code. The study also surveyed their findings with 71 real App developers to understand how developers perceive the exception handling that leads to bug hazards (i.e., the potential of introducing bugs).
% Jens: shortened this a bit to create some space 
%The study found that several unhandled exception can lead to bug hazards such as cross-type exception (e.g., \texttt{OutOfMemoryError}), undocumented runtime exceptions raised and undocumented checked exceptions signalled by native C code. The study also found that errors in programming logic expectation such as \texttt{NullPointerException} are responsible for the majority of bugs in the investigated programs.


\section{Conclusion}

In this paper, we have investigated the question whether information harvested from online data sources can be used to check and improve the soundness of static program models. The short answer is yes. Although we found only six edges that \textit{Soot} could not extract, we notice the rather large number of edges we found. The main reason that we did not found more results is not the lack of suitable information, rather than the small size of our data set. But by using a smaller data set we were able to manually check and investigate the edges we detected. 

An extended study with a larger dataset is an interesting topic for future research. One particular interesting issue is the study of call graphs that cover projects and libraries, including frameworks known for their heavy use of reflection (plugin-based systems, dependency injection) and the Java core libraries. We have noticed a large number of reflective invocation where call site and target were in different libraries. One potential problem here is that it is still challenging to build comprehensive and sufficiently precise static models for real-world programs that include all library dependencies, although new algorithms and tools are under development to address scalability issues~\cite{dietrich2015giga}.

A particular interesting fact is how different the edges detected are. As discussed above, some could be detected by tools implementing state of the art reflection analysis \cite{Livshits2005Reflection,bodden2011taming,smaragdakis2015more}, while for others this seems highly unlikely due to the level of sophistication of the use of reflection.  Building a dataset containing a reasonable number of these and other usage patterns to be used to benchmark static analysis tools is another interesting topic for future research.


\section*{Acknowledgement}

[tba after review]
% The authors would like to thank Georgios Gousios and Davy Landman for their comments.

\bibliographystyle{plain}
\bibliography{references}


% that's all folks
\end{document}


