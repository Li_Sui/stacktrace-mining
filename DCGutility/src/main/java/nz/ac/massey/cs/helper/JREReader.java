package nz.ac.massey.cs.helper;

import javassist.ClassPool;
import javassist.CtClass;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class JREReader {


    public static void main(String[] args) throws Exception{

        Iterator it = FileUtils.iterateFiles(new File("jre1.8/lib"),null,true);
        StringBuilder sb=new StringBuilder();
        while(it.hasNext()) {
            File f = (File) it.next();
            if (f.getName().endsWith(".jar")) {
                ZipFile zipFile = new ZipFile(f.getPath());

                Enumeration<? extends ZipEntry> entries = zipFile.entries();

                while(entries.hasMoreElements()){
                    ZipEntry entry = entries.nextElement();
                    if(entry.getName().endsWith(".class")) {
                        InputStream stream = zipFile.getInputStream(entry);
                        readClass(IOUtils.toByteArray(stream),sb);
                    }
                }


            }
        }

        FileUtils.write(new File("jreClasses.txt"),sb.toString(), Charset.defaultCharset());

    }



    private static void readClass(byte[] b,StringBuilder sb){
        ClassPool pool = ClassPool.getDefault();
        CtClass clazz = null;
        try {
            clazz = pool.makeClass(new ByteArrayInputStream(b));

            if(clazz!=null){
                sb.append(clazz.getName()+"\n");

            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (clazz != null) {
                clazz.detach();
            }
        }


    }
}
