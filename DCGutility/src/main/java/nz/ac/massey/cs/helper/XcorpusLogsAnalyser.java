package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;

/**
 * analysis xcorpus logs file to see how often tests time out
 */
public class XcorpusLogsAnalyser {

    public static void main(String[] args) throws Exception{

        String logsDir="/home/lsui/projects/recall_study/xcorpus-experiment/data";
        Iterator it = FileUtils.iterateFiles(new File(logsDir),null,true);
        Map<String, Coverage> map =new HashMap<>();
        while(it.hasNext()){
            File f = (File) it.next();
//            if(f.getName().contains("coveragelog")){
//                String project= f.getParentFile().getParentFile().getName();
//
//                List<String> timeout=new ArrayList<String>();
//                sanitiseLog(FileUtils.readFileToString(f, Charset.defaultCharset()),timeout);
//                System.out.println(project+" has "+timeout.size()+ " numbers of time out");
//
//            }

//            if(f.getName().contains("driverLog.txt")){
//                String project= f.getParentFile().getParentFile().getName();
//
//                List<String> timeout=new ArrayList<String>();
//                sanitiseLog(FileUtils.readFileToString(f, Charset.defaultCharset()),timeout);
//                System.out.println(project+" has "+timeout.size()+ " numbers of time out");
//
//            }

            if(f.getName().equals("report.csv")){


                String project= f.getParentFile().getParentFile().getParentFile().getName();
                if(project.equals(".xcorpus")){
                    project=f.getParentFile().getParentFile().getParentFile().getParentFile().getName();
                }

                if(map.containsKey(project)){
                    map.get(project).addCovered(getcoverage(FileUtils.readFileToString(f, Charset.defaultCharset())).getCovered());
                    map.get(project).addMissed(getcoverage(FileUtils.readFileToString(f, Charset.defaultCharset())).getMissed());
                }else{
                    map.put(project,getcoverage(FileUtils.readFileToString(f, Charset.defaultCharset())));
                }




            }
        }

        for(String p: map.keySet()){
            System.out.println(p+","+map.get(p).getCovered()/(map.get(p).getMissed()+map.get(p).getCovered()));
        }
    }

    public static void sanitiseLog(String content,List<String> timeout){
        Stack<String> stack=new Stack<String>();
        String[] lines=content.split(System.getProperty("line.separator"));
        for(String line: lines){
            if(line.contains("[xcorpus]")){
                stack.push(line);
            }
            if(line.contains("Java Result: -1")){
                timeout.add(stack.peek());
            }
        }
    }

    private static Coverage getcoverage(String readFileToString) {
        String[] lines =readFileToString.split(System.getProperty("line.separator"));
        double missBranches=0;
        double coveredBranches=0;
        for(int i=1;i<lines.length;i++){
            missBranches= missBranches+Double.parseDouble(lines[i].split("\\,")[5]);
            coveredBranches=coveredBranches+ Double.parseDouble(lines[i].split("\\,")[6]);

        }
        return new XcorpusLogsAnalyser().new Coverage(coveredBranches,missBranches);
    }

    public class Coverage{
        private double covered;
        private double missed;

        public Coverage(double covered,double missed){
            this.covered=covered;
            this.missed=missed;
        }

        public void addCovered(double covered) {
            this.covered = this.covered+covered;
        }

        public void addMissed(double missed) {
            this.missed = this.covered+missed;
        }

        public double getCovered() {
            return covered;
        }

        public double getMissed() {
            return missed;
        }
    }
}
