package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.BiconnectivityInspector;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.traverse.DepthFirstIterator;
import org.jgrapht.util.SupplierUtil;

import java.io.File;
import java.nio.charset.Charset;
import java.util.*;

/**
 * noise source:
 * nz.ac.massey.cs.util.Util+getInstance+()Lnz/ac/massey/cs/util/Util;
 * nz.ac.massey.cs.util.Util+<init>+()V
 *
 * nz.ac.massey.cs.util.Util+<clinit>+()V
 *
 * nz.ac.massey.cs.util.Util+getClassLoader+(Ljava/lang/Class;)Ljava/lang/String;
 */

public class HelloJGraphT {
    static String dcgDir="jmoney-dcg";
    static String cgFileregex = "cg[0-9]+\\.csv";
    static String entryPointregex="Driver[0-9]+\\+main\\+\\(\\[Ljava/lang/String;\\)V";

    public static void main(String args[]) throws Exception{


        List<String> noiseList= Arrays.asList("nz.ac.massey.cs.util.Util+getInstance+()Lnz/ac/massey/cs/util/Util;"
                ,"nz.ac.massey.cs.util.Util+<init>+()V","nz.ac.massey.cs.util.Util+<clinit>+()V"
                ,"nz.ac.massey.cs.util.Util+getClassLoader+(Ljava/lang/Class;)Ljava/lang/String;");
        //construct dynamic call graph
        Graph<String, DefaultEdge> directedGraph =
                new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        Iterator it = FileUtils.iterateFiles(new File(dcgDir),null,true);
        Set<String> entryPointSet= new HashSet<>();
        while(it.hasNext()) {
            File f = (File) it.next();
            if(f.getName().matches(cgFileregex)) {//cg1548303675140.csv
                String string=FileUtils.readFileToString(f, Charset.defaultCharset());
                for(String edge: string.split(System.getProperty("line.separator"))){
                    String caller=CGDiff.parseDynamicCall(edge.split("\t")[0]);
                    String callee=CGDiff.parseDynamicCall(edge.split("\t")[1]);
                    if(caller.matches(entryPointregex)){
                        entryPointSet.add(caller);
                    }
                        directedGraph.addVertex(caller);
                        directedGraph.addVertex(callee);
                        directedGraph.addEdge(caller, callee);
                }
            }
        }

        //remove noise
        for(String noise: noiseList) {
            directedGraph.removeVertex(noise);
        }

//        BiconnectivityInspector biconnectivityInspector=new BiconnectivityInspector(directedGraph);
//        System.out.println( biconnectivityInspector.getBridges());

//        DijkstraShortestPath dijkstraShortestPath
//                = new DijkstraShortestPath(directedGraph);
//        List<String> shortestPath = dijkstraShortestPath
//                .getPath(entry,"java.util.Collections$UnmodifiableCollection+toArray+()[Ljava/lang/Object;").getVertexList();
//        System.out.println(shortestPath);
//        Set<String> dset =new HashSet<>();
//        for(String entry: entryPointSet){
//            Iterator<String> iter = new DepthFirstIterator<>(directedGraph,entry);
//
//            while (iter.hasNext()) {
//                if(iter.next().toString().equals("java.lang.Object+toString+()Ljava/lang/String;")){
//                    System.out.println("find");
//                    break;
//                }
//                //dset.add(iter.next());
//            }
//        }
//
//
//
        Iterator<String> iter2 = new DepthFirstIterator<>(directedGraph,"nz.ac.massey.cs.util.Util+<clinit>+()V");

        while (iter2.hasNext()) {
            System.out.println(iter2.next());
        }



    }

}
