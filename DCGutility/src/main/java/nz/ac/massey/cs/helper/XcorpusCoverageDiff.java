package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class XcorpusCoverageDiff {

    public static void main(String[] args) throws Exception{
List<String> projects=Arrays.asList("velocity-1.6.4","ApacheJMeter_core-3.1","lucene-4.3.0","jena-2.6.3","pmd-4.2.5","wct-1.5.2","asm-5.2","mockito-core-2.7.17","jrat-0.6","marauroa-3.8.1","javacc-5.0","informa-0.7.0-alpha2","nekohtml-1.9.14","guava-21.0","jFin_DateMath-R1.0.1","trove-2.1.0","quartz-1.8.3","jrefactory-2.9.19","commons-collections-3.2.1","jfreechart-1.0.13","tomcat-7.0.2","checkstyle-5.1","drools-7.0.0.Beta6","findbugs-1.3.9","jgrapht-0.8.1","fitjava-1.1","log4j-1.2.16","openjms-0.7.7-beta-1","oscache-2.4.1","weka-3-7-9","htmlunit-2.8","castor-1.3.1");


        System.out.println("project,builtinTest-instructionCoverage1,builtinTest-branchCoverage1,generatedTest-instructionCoverage1,generatedTest-branchCoverage1");
        for(String p: projects){
            System.out.print(p+",");
            for(int i=1;i<6;i++) {

                String builti = "/home/lsui/projects/recall_study/coverage-xcorpus/xcorpus-coverage"+i +"/" + p + "/"+ p + "-coverage-builtinTest.zip";
                String generated = "/home/lsui/projects/recall_study/coverage-xcorpus/xcorpus-coverage"+i + "/" + p + "/" + p + "-coverage-generatedTest.zip";
                String combained = "/home/lsui/projects/recall_study/coverage-xcorpus/xcorpus-coverage"+i + "/" + p + "/" + p + "-coverage-combained.zip";
                System.out.print( parseZip(new File(builti)) + "," + parseZip(new File(generated))+",");

            }
            System.out.println();

        }

    }

    private static String parseZip(File f) throws Exception{

            ZipFile zipFile = new ZipFile(f.getPath());

            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            String result="";
            while(entries.hasMoreElements()){
                ZipEntry entry = entries.nextElement();
                if(entry.getName().equals("report.csv")) {
                    InputStream stream = zipFile.getInputStream(entry);
                    String content=IOUtils.toString(stream, Charset.defaultCharset());
                    double instruction_miss=0;
                    double instruction_cover=0;
                    double branch_miss=0;
                    double branch_cover=0;
                    String[] csv=content.split(System.lineSeparator());
                    for(int i=1;i<csv.length;i++){
                       instruction_miss=instruction_miss+Double.parseDouble( csv[i].split("\\,")[3]);
                       instruction_cover=instruction_cover+Double.parseDouble( csv[i].split("\\,")[4]);
                       branch_miss=branch_miss+Double.parseDouble(csv[i].split("\\,")[5]);
                       branch_cover=branch_cover+Double.parseDouble( csv[i].split("\\,")[6]);
                    }
                    double total_instruction=instruction_cover+instruction_miss;
                    double total_branch=branch_cover+branch_miss;

                    double instruction_coverage=instruction_cover/total_instruction;
                    double branch_coverage=branch_cover/total_branch;
                    result=String.format("%.2f",instruction_coverage*100)+","+String.format("%.2f",branch_coverage*100);
                    break;
                }
            }
            return result;

    }

}
