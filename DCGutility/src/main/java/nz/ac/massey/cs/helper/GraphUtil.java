package nz.ac.massey.cs.helper;

import java.util.HashSet;
import java.util.Set;

public class GraphUtil {

    public static Set<String> diff(GraphV dcg, GraphV scg){
        Set<String> TP=new HashSet<>();
        Set<String> FN=new HashSet<>();

        //find TP
        for(String vertex: (Set<String>)dcg.getCallGraph().keySet()){
            if(scg.contains(vertex)){
                Set<String> dr=dcg.bfs(vertex);
                //find same elements in both collections
                dr.retainAll(scg.bfs(vertex));

                if(!dr.isEmpty()){
                    TP.add(vertex);
                    TP.addAll(dr);
                }
            }
        }
        System.out.println("tp: "+TP.size());

        //find fn
        for(String vertex: (Set<String>)dcg.getCallGraph().keySet()){
            if(!TP.contains(vertex)){
                FN.add(vertex);
            }
        }

        System.out.println("fn: "+FN.size());
        System.out.println(FN);
        return FN;
    }
}
