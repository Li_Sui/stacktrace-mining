package nz.ac.massey.cs.helper;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class AppClassVisitor extends ClassVisitor {
    private boolean isTarget=false;
    public String className;
    public Set<String> entrypoint;
    public AppClassVisitor(Set<String> entrypoint) {
        super(Opcodes.ASM5);
        this.entrypoint=entrypoint;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        className = name.replace("/",".");
        if(interfaces.length>0){
            List<String> list =Arrays.asList(interfaces);
            if(list.contains("java/lang/Runnable")){
                isTarget=true;
            }

        }

        if(superName.equals("java/lang/Thread")){
            isTarget=true;
        }

    }



    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        if(isTarget && name.equals("run") && desc.equals("()V")) {
            entrypoint.add(className+"+run+()V");
        }

        return super.visitMethod(access,name,desc,signature,exceptions);
    }
}