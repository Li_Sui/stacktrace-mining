package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class MergeStats {

    public static void main(String[] args) throws Exception{

        String[] olddriverCoverage=FileUtils.readFileToString(new File("/home/lsui/projects/recall_study/xcorpus-coverage-study/coverage.csv"), Charset.defaultCharset()).split(System.getProperty("line.separator"));
        String[] evosuiteCoverage=FileUtils.readFileToString(new File("/home/lsui/projects/recall_study/xcorpus-coverage-study/evosuite.csv"), Charset.defaultCharset()).split(System.getProperty("line.separator"));
       // String[] numberClasses=FileUtils.readFileToString(new File("/home/lsui/projects/coverage-prestudy/stats.csv"), Charset.defaultCharset()).split(System.getProperty("line.separator"));
       // String[] timeout=FileUtils.readFileToString(new File("/home/lsui/projects/coverage-prestudy/timeout.csv"), Charset.defaultCharset()).split(System.getProperty("line.separator"));
        String[] newDriverCoverage=FileUtils.readFileToString(new File("/home/lsui/projects/recall_study/xcorpus-coverage-study/driver-coverage.csv"), Charset.defaultCharset()).split(System.getProperty("line.separator"));

        HashMap<String,String> olddriverMap=new HashMap<String,String>();
        HashMap<String,String> newdriverMap=new HashMap<String,String>();
        HashMap<String,String> evosuiteMap=new HashMap<String,String>();
        //HashMap<String,String> numberClassMap=new HashMap<String,String>();
        //HashMap<String,String> timeoutMap=new HashMap<String,String>();
        for(String line:olddriverCoverage){
            olddriverMap.put(line.split(",")[0],line.split(",")[1]);
        }

        for(String line:newDriverCoverage){
            newdriverMap.put(line.split(",")[0],line.split(",")[1]);
        }
        //System.out.println(driverMap);
        for(String line:evosuiteCoverage){
            evosuiteMap.put(line.split(",")[0],line.split(",")[1]);
        }
        //System.out.println(evosuiteMap);
//        for(String line:numberClasses){
//            numberClassMap.put(line.split(",")[0],line.split(",")[1]+","+line.split(",")[2]);
//        }
//        //System.out.println(numberClassMap);
//        for(String line:timeout){
//            timeoutMap.put(line.split(",")[0],line.split(",")[1]);
//        }
//       //System.out.println(timeoutMap);
//
        System.out.println("project,Evosuite-coverage,driver-coverage-1,driver-coverage-2");
        for(String project: newdriverMap.keySet()){


            System.out.println(project+","+evosuiteMap.get(project)+","+olddriverMap.get(project)+","+newdriverMap.get(project));
        }
    }
}
