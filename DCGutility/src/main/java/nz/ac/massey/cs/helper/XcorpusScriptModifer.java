package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Iterator;

public class XcorpusScriptModifer {
    public static String xcorpusDir="/home/lsui/projects/recall_study/xcorpus/data";
    static String test="/home/lsui/projects/xcorpus/data/qualitas_corpus_20130901/aoi-2.8.1/.xcorpus/output/junit-report";
    public static void main(String[] args) throws Exception {
        Iterator<File> it = FileUtils.iterateFiles(new File(xcorpusDir),null,true);

        while(it.hasNext()){
            File f = it.next();
            modifyBuildScript(f);
            //searchForError(f);
            // generateZipScript(f);

        }

    }

    public static void searchForError(File f) throws Exception{
        if(f.getName().endsWith(".xml")){
            String content=FileUtils.readFileToString(f);
            String[] lines=content.split(System.getProperty("line.separator"));

            for(int i=0;i<lines.length;i++){
                String line=lines[i];
                if(line.contains("<error message=")) {
                    System.out.println("found error in "+lines[i-1]+"---error: "+line);
                }
            }
        }
    }

//	public static void generateZipScript(File f) throws Exception{
//		if(f.getName().equals("exercise.xml")){
//			File out=new File(f.getAbsolutePath().replace("exercise.xml", "")+"tmp.xml");
//
//		}
//	}

    public static void modifiyGeneratedTest(File f) throws Exception{
        if(f.getName().endsWith("_ESTest.java")){
            String content=FileUtils.readFileToString(f);
            String[] lines=content.split(System.getProperty("line.separator"));
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<lines.length;i++){
                String line=lines[i];
                if(line.contains("@Test(timeout")) {
                    sb.append("\t@Test(timeout=300000)\n");
                }else {
                    sb.append(line+"\n");
                }
            }
            FileUtils.writeStringToFile(f, sb.toString());
        }
    }

    public static void modifyBuildScript(File f) throws Exception{
        if(f.getName().equals("exercise.xml")){
            System.out.println(f.getParentFile().getParentFile().getName());
            String content=FileUtils.readFileToString(f);
            String[] lines=content.split(System.getProperty("line.separator"));
            StringBuilder sb=new StringBuilder();
            for(int i=0;i<lines.length;i++){
                String line=lines[i];
                if(line.contains("<junit dir=\"${basedir}/temp\"")) {
                    sb.append("\t\t<junit dir=\"${basedir}/temp\" timeout=\"${junit.timeout}\" maxmemory=\"${junit.maxmemory}\" printsummary=\"on\" haltonfailure=\"${xcorpus.haltOnFailure}\" haltonerror=\"${xcorpus.haltOnError}\" fork=\"true\">\n");
                }else {
                    sb.append(line+"\n");
                }

            }
            FileUtils.writeStringToFile(f, sb.toString());
        }
    }
}
