package nz.ac.massey.cs.helper;

import javassist.ClassPool;
import javassist.CtBehavior;
import javassist.CtClass;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.objectweb.asm.ClassReader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class EntryPointExtractor {


    public static void main(String[] args) throws Exception{
        String inputDir =args[0];

        String outputFile =args[1];
        // iterate a dir to find all zips and jars
        Iterator it = FileUtils.iterateFiles(new File(inputDir),null,true);
        Set<String> callsite=new HashSet<>();
        while(it.hasNext()){
            File f = (File) it.next();
            parseZip(f,callsite);
        }
        // extract call sites from java runtime(rt.jar)

        StringBuilder sb =new StringBuilder();
        for(String c: callsite){
            sb.append(c+"\n");
        }
        FileUtils.writeStringToFile(new File(outputFile),sb.toString(), Charset.defaultCharset());
    }

    private static void parseZip(File f,Set<String> entrypoint) {
        if(f.getName().endsWith(".jar") || f.getName().equals("bin.zip")){
            try {
                ZipFile zipFile = new ZipFile(f.getPath());

                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                System.out.println("extracting:" + f.getPath());
                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().endsWith(".class")) {
                        InputStream stream = zipFile.getInputStream(entry);
                        ClassReader reader = new ClassReader(stream);
                        AppClassVisitor cv = new AppClassVisitor(entrypoint);
                        reader.accept(cv, 0);
                    }
                }
            }catch (Exception e){

            }

        }
    }


}
