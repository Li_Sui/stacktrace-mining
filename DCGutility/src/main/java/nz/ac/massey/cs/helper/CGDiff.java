package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.DepthFirstIterator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

public class CGDiff {
    static String dcgDir="jmoney-dcg";
    static String doopFile_base="/home/lsui/projects/doop-jmoney/results/base/Reachable.csv";
    static String doopFile_callsite="/home/lsui/projects/doop-jmoney/results/callsite/Reachable.csv";
    static String doopFile_reflection="/home/lsui/projects/doop-jmoney/results/reflection/Reachable.csv";

    static String cgFileregex = "cg[0-9]+\\.csv";
    static String entryPointregex="Driver[0-9]+\\+main\\+\\(\\[Ljava/lang/String;\\)V";

    public static void main(String[] args) throws Exception{





        Set<String> dynamicSet=getdynamicReachableSet();
        Set<String> doopSet_base=getDoopReachableSet(doopFile_base);
        Set<String> doopSet_callsite=getDoopReachableSet(doopFile_callsite);
        Set<String> doopSet_reflection=getDoopReachableSet(doopFile_reflection);

        Set<String> FN_base =new HashSet<>();
        Set<String> FN_callsite =new HashSet<>();
        Set<String> FN_reflection =new HashSet<>();


        for(String call:dynamicSet){
            if(!doopSet_base.contains(call)){
                FN_base.add(call);
            }
            if(!doopSet_callsite.contains(call)){
                FN_callsite.add(call);
            }
            if(!doopSet_reflection.contains(call)){
                FN_reflection.add(call);
            }
        }

        System.out.println("dynamic reachable:"+dynamicSet.size());
        System.out.println("-------------------------------------");
        System.out.println("doop reachable -base:"+doopSet_base.size());
        System.out.println("FN-base:"+FN_base.size());
        System.out.println("-------------------------------------");
        System.out.println("doop reachable -callsite:"+doopSet_callsite.size());
        System.out.println("FN-callsite:"+FN_callsite.size());
        System.out.println("-------------------------------------");
        System.out.println("doop reachable -reflection:"+doopSet_reflection.size());
        System.out.println("FN-reflection:"+FN_reflection.size());
        System.out.println("-------------------------------------");

        FN_callsite.removeAll(FN_base);
        System.out.println(FN_callsite);

    }

    private static Set<String> getDoopReachableSet(String file) {
        Set<String> reachable = new HashSet<>();
        try {

            String[] reachableSet = FileUtils.readFileToString(new File(file), Charset.defaultCharset()).split(System.getProperty("line.separator"));
            for (String edge : reachableSet) {
                reachable.add(parseDoopCall(edge));
            }
        }catch(IOException e){

        }
        return reachable;
    }

    private static Set<String> getdynamicReachableSet() throws Exception{
        Graph<String, DefaultEdge> directedGraph =
                new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);
        Set<String> dSet=new HashSet<>();
        Set<String> entryPointSet= new HashSet<>();
        Iterator it = FileUtils.iterateFiles(new File(dcgDir),null,true);

//        while(it.hasNext()) {
//            File f = (File) it.next();
//            if(f.getName().matches(cgFileregex)) {
//                String string=FileUtils.readFileToString(f, Charset.defaultCharset());
//                for(String edge: string.split(System.getProperty("line.separator"))){
//                    String caller=parseDynamicCall(edge.split("\t")[0]);
//                    String callee=parseDynamicCall(edge.split("\t")[1]);
//                    if(caller.matches(entryPointregex)){
//                        entryPointSet.add(caller);
//                        dSet.add(caller);
//                    }
//                    dcg.add(caller,callee);
//                }
//            }
//        }
//
//        for(String entry: entryPointSet){
//
//            Set<String> r= dcg.bfs(entry);
//            for(String call:r){
//                if(!call.startsWith("nz.ac.massey.cs.util.Util")){
//                    dSet.add(call);
//                }
//            }
//        }

        while(it.hasNext()) {
            File f = (File) it.next();
            if(f.getName().matches(cgFileregex)) {
                String string=FileUtils.readFileToString(f, Charset.defaultCharset());
                for(String edge: string.split(System.getProperty("line.separator"))){
                    String caller=CGDiff.parseDynamicCall(edge.split("\t")[0]);
                    String callee=CGDiff.parseDynamicCall(edge.split("\t")[1]);
                    if(caller.matches(entryPointregex)){
                        entryPointSet.add(caller);

                    }
                    directedGraph.addVertex(caller);
                    directedGraph.addVertex(callee);
                    directedGraph.addEdge(caller,callee);
                }
            }
        }

        for(String entry: entryPointSet){
            Iterator<String> iter = new DepthFirstIterator<>(directedGraph,entry);
            while (iter.hasNext()) {
                dSet.add(iter.next());
            }
        }
        return dSet;
    }

    public static String parseDynamicCall(String call){
        String classLoaderName=call.split("\\+")[0];
        String className=call.split("\\+")[1];
        String methodName=call.split("\\+")[2];
        String parameter_returnType=call.split("\\+")[3];
        return className+"+"+methodName+"+"+parameter_returnType;
    }

    public static String parseDoopCall(String call){

        String className="";
        if(call.contains("<register-finalize")){
            className= StringUtils.substringBetween(call,"<register-finalize <",":");
        }else{
            className=StringUtils.substringBetween(call,"<",":");;
        }

        String returnType= parseString(StringUtils.substringBetween(call,": ","(").split("\\s")[0]);
        String methodName= StringUtils.substringBetween(call,": ","(").split("\\s")[1];
        String doopParameters=StringUtils.substringBetween(call,"(",")");
        String parameters="";
        if(doopParameters.contains(",")) {
            for (String p : doopParameters.split(",")){
                parameters=parameters+parseString(p);
            }
        }else{
            parameters=parseString(doopParameters);
        }
        if(doopParameters.equals("")){
            parameters="";
        }

        return className+"+"+methodName+"+("+parameters+")"+returnType;
    }

    public static String parseString(String s){
        String tmp="";
        if(s.equals("void")) {
            return "V";
        }
        if(s.equals("int")) {
            return "I";
        }
        if(s.equals("int[]")) {
            return "[I";
        }
        if(s.equals("byte")){
            return "B";
        }
        if(s.equals("byte[]")){
            return "[B";
        }
        if(s.equals("long")){
            return "J";
        }
        if(s.equals("long[]")){
            return "[J";
        }
        if(s.equals("float")){
            return "F";
        }
        if(s.equals("float[]")){
            return "[F";
        }
        if(s.equals("double")){
            return "D";
        }
        if(s.equals("double[]")){
            return "[D";
        }
        if(s.equals("short")){
            return "S";
        }
        if(s.equals("short[]")){
            return "[S";
        }
        if(s.equals("char")){
            return "C";
        }
        if(s.equals("char[]")){
            return "[C";
        }
        if(s.equals("boolean")){
            return "Z";
        }
        if(s.equals("boolean[]")){
            return "[Z";
        }


        tmp="L"+s.replaceAll("\\.","/")+";";

        if(tmp.contains("[]")){
            tmp="["+tmp.replaceAll("\\[\\]","");
        }
        return tmp;
    }

}
