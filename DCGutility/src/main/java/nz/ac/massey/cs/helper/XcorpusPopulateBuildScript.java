package nz.ac.massey.cs.helper;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Iterator;

public class XcorpusPopulateBuildScript {


    public static void main(String[] args) throws Exception{
        String xcorpusDir="/home/lsui/projects/recall_study/xcorpus-experiment";

        Iterator it = FileUtils.iterateFiles(new File(xcorpusDir),null,true);
        int count=0;
        while(it.hasNext()) {
            File f = (File) it.next();
            if (f.getName().equals("exercise.xml")) {

                    String outputdir = f.getParent();
                    //f.delete();
                    FileUtils.copyFile(new File("entrypoint.xml"), new File(outputdir + "/entrypoin.xml"));
                    count++;

            }
        }
        System.out.println(count);
    }
}
