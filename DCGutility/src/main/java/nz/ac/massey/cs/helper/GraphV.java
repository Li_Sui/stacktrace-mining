package nz.ac.massey.cs.helper;


import java.util.*;
public class GraphV<V>{

    private Map<V,List<V>> neighbors = new HashMap<V,List<V>>();

    public void add (V vertex) {
        if (neighbors.containsKey(vertex)) return;
        neighbors.put(vertex, new ArrayList<V>());
    }

    public boolean contains (V vertex) {
        return neighbors.containsKey(vertex);
    }

    public Map<V,List<V>> getCallGraph(){ return neighbors; }

    public void add (V from, V to) {
        this.add(from); this.add(to);
        neighbors.get(from).add(to);
    }

    public Set<V> bfs(V start) {
        Set<V> reachables=new HashSet<>();
        Queue<V> q = new LinkedList<V>();
        q.offer(start);
        while (!q.isEmpty()) {
            V v = q.remove();
            for (V neighbor: neighbors.get(v)) {
                if (reachables.contains(neighbor)){
                    continue;
                }
                reachables.add(neighbor);
                q.offer(neighbor);
            }
        }
        return reachables;
    }





}