package servLoader;

import java.util.ServiceLoader;

/**
 * simple example using serviceLoader to load different implementation of a service.
 * check the edge is created and map to {@benchmark.servLoader.ServiceImple1}.
 * META-IF contains "benchmark.servLoader.ServiceImple1"
 * 
 * @author Li Sui
 *
 */
public class ServiceLoaderTest {

	public static void main(String[] args) {
		ServiceLoader<ServiceIF> serviceLoader =ServiceLoader.load(ServiceIF.class);
		
		for(ServiceIF s : serviceLoader){
			s.foo();
		}
	}

}
