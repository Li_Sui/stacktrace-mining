package basic;

/**
 * supplement class of {@benchmark.basics.Basic1} and {@benchmark.basics.Basic2}
 * 
 * @author Li Sui
 *
 */
public class Rectangle implements Shape{

	@Override
	public void draw(){}

}
