package basic;

/**
 * 
 * dynamic dispatch for invokeinterface
 * @author Li Sui
 *
 */
public class Basic2 {

	public static void  main(String[] args){
		Shape shape = new RoundedRectangle();
		shape.draw();
	}
	
}
