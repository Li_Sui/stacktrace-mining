package basic;

/**
 * dynamic dispatch for invokevirtual
 * 
 * @author Li Sui
 *
 */
public class Basic1 {

	public static void  main(String[] args){
		Rectangle rect = new RoundedRectangle();
		rect.draw();	
	}
}
