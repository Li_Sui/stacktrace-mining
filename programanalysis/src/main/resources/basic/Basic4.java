package basic;

/**
 * representation of synthetic bridge methods created for accessing private method of enclosing classes
 * @author Li Sui
 *
 */
public class Basic4 {

	private void outerFoo(){

	}
	
	public static void main(String[] args){
		Inner i=new Basic4().new Inner();
		i.innerFoo();
	}	
	
	public class Inner {
		
		public void innerFoo() {
			outerFoo();
		}
	}
}
