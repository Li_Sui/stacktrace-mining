package basic;

/**
 * supplement interface of {@benchmark.basics.Basic1} and {@benchmark.basics.Basic2}
 * 
 * @author Li Sui
 *
 */
public interface Shape {
	
	public void draw();
}
