package basic;

/**
 * representation of synthetic bridge methods created for co-variant return types 
 * @author Li Sui
 *
 */
public class Basic3 {

	public static void main(String[] args){
		new Basic3().new Inner().get();
	}
	
	public class Inner extends SuperInner{
		
		public Inner get(){
			return this;
		}
	}
	public class SuperInner{
		
		public SuperInner get(){
			return this;
		}
	}
}
