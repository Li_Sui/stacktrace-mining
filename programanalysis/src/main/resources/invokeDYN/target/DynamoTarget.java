package invokeDYN.target;

/**
 * target class for {@benchmark.invokeDYN.DynamoClient}
 * 
 * @author Li Sui
 *
 */
public class DynamoTarget {

	public void foo(String o){
		System.out.println(o);
	}
}
