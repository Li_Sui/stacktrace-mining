package invokeDYN.client;

import invokeDYN.target.DynamoTarget;

/**
 * invokedynamic example, using dynamo to transform bytcode from invokevirtual to invokedynamic
 * @author Li Sui
 *
 */
public class DynamoClient {
	  public static void main(String[] args) {
	        new DynamoTarget().foo("Hello");
	    }
}
