package refl;

import java.lang.reflect.Method;

/**
 * flow-sensitivity applied. Check whether spurious edges are created.
 * 
 * @author Li Sui
 *
 */
public class Reflection9 {

	public static void main(String[] args) throws Exception{
		Method m;
		String arg="id";
		if(arg.equals("id")){
			m = Class.forName("refl.Reflection9").getDeclaredMethod("id", new Class[]{java.lang.Object.class});
		}else{
			m = Class.forName("refl.Reflection9").getDeclaredMethod("foo", new Class[]{java.lang.Object.class});
		}
		m.invoke(new Reflection9(), new Object[]{new Object()});
	}
	
	
	public Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}

}
