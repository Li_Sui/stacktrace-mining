package refl;

import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Simple reflection, method name read from file. both methods are non-static
 * check whether tool can over-approximate/under-approximate the result if method name is "hidden"
 * 
 * @author Li Sui
 *
 */
public class Reflection6 {

	public static void main(String[] args) throws Exception {
		String methodName = new String(Files.readAllBytes(Paths.get("src/main/resources/reflection-methodName.txt"))).trim();
		
		Method m = Class.forName("refl.Reflection6").getDeclaredMethod(methodName, new Class[]{java.lang.Object.class});
		m.invoke(new Reflection6(), new Object[]{new Object()});
	}
	
	public Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}
}
