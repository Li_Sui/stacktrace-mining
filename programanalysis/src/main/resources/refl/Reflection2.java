package refl;


import java.lang.reflect.Method;

/**
 * Simple reflection, hardcoded classname , 
 * one possible target method with a matching name, method name is computed via string concatenation,
 * but compiler can resolve this using constant folding
 * 
 * @author Li Sui
 *
 */
public class Reflection2 {

	
	public static void main(String[] args) throws Exception {
		
		String reflectedMethod="i"+"d";	
		Method m = Class.forName("refl.Reflection2").getDeclaredMethod(reflectedMethod, new Class[]{java.lang.Object.class});
		m.invoke(new Reflection2(), new Object[]{new Object()});
	}

	public Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}

}
