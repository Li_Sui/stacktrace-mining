package refl;


import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Simple reflection, class name read from file.
 * check whether tool can over-approximate/under-approximate the result if class name is "hidden"
 * 
 * @author Li Sui
 *
 */

public class Reflection4 {


	public static void main(String[] args) throws Exception {

		String className = new String(Files.readAllBytes(Paths.get("src/main/resources/reflection-className.txt"))).trim();
		Class<?> clazz = Class.forName(className);
		
		// Jens: could have two scenarios here
		// if clazz is declared as Class , we could only approximate by using all methods with name id (all classes from all libs)
		// if clazz was declared as Class<Reflection4> , we could use a tider over-approximation
		
		Method m = clazz.getDeclaredMethod("id", new Class[]{java.lang.Object.class});
		m.invoke(new Reflection4(), new Object[]{new Object()});
	}
	
	public Object id(Object v){
		return v;
	}
}
