package refl;

import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Simple reflection, method name read from file. one method is static method and another one is not.
 * check whether tool can over-approximate/under-approximate the result if method name is "hidden"
 * 
 * @author Li Sui
 *
 */
public class Reflection5 {
	
	public static void main(String[] args) throws Exception {
		String methodName = new String(Files.readAllBytes(Paths.get("src/main/resources/reflection-methodName.txt"))).trim();
		
		Method m = Class.forName("refl.Reflection5").getDeclaredMethod(methodName, new Class[]{java.lang.Object.class});
		m.invoke(new Reflection5(), new Object[]{new Object()});
	}
	
	public Object id(Object v){
		return v;
	}
	
	public static Object foo(Object v){
		return v;
	}

}
