package refl;


import java.lang.reflect.Method;
/**
 * Simple reflection, hardcoded class and method name , one possible target method with a matching name, one that does not match
 * 
 * @author Li Sui
 *
 */
public class Reflection1 {

	public static void main(String[] args) throws Exception {
		Method m = Class.forName("refl.Reflection1").getDeclaredMethod("id", new Class[]{java.lang.Object.class});
		m.invoke(new Reflection1(), new Object[]{new Object()});
	}
	
	public  Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}
	
}
