package refl;


import java.lang.reflect.Method;

/**
 * Two string literal is used within the method with the callsite, 
 * but only one is used as method name (one is use in methodFoo="foo"). 
 * Check whether spurious edges are created.
 * 
 * @author Li Sui
 *
 */
public class Reflection7 {

	public static void main(String[] args)throws Exception{
		
		String methodName="id";
		String methodFoo="foo";
		Method m = Class.forName("refl.Reflection7").getDeclaredMethod(methodName, new Class[]{java.lang.Object.class});
		m.invoke(new Reflection7(), new Object[]{new Object()});
	}
	
	public Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}


}
