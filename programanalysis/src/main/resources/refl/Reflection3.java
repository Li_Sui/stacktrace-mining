package refl;

import java.lang.reflect.Method;

/**
 * Simple reflection, hardcoded class- and method name, four possible target methods (overloading), 
 * check whether tool can reason about parameter types or resorts to over-approximation (introduces spurious edges)
 * 
 * @author Li Sui
 *
 */
public class Reflection3 {
	
	
	public static void main(String[] args) throws Exception {
		Method m = Class.forName("refl.Reflection3").getDeclaredMethod("id", new Class[]{java.lang.String.class});
		m.invoke(new Reflection3 (), new Object[]{new String("hello")});
	}

	public Object id(Integer v){
		return v;
	}
	
	public Object id(String v){
		return v;
	}
	

}
