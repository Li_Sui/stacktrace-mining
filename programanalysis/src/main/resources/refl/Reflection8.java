package refl;

import java.lang.reflect.Method;

/**
 * Two string literal is used within the method with the callsite, 
 * flow-sensitivity applied. Check whether spurious edges are created.
 * 
 * @author Li Sui
 *
 */
public class Reflection8 {

public static void main(String[] args)throws Exception{
		
		String methodName="foo";
		methodName="id";
		
		Method m = Class.forName("refl.Reflection8").getDeclaredMethod(methodName, new Class[]{java.lang.Object.class});
		m.invoke(new Reflection8(), new Object[]{new Object()});
	}
	
	public Object id(Object v){
		return v;
	}
	
	public Object foo(Object v){
		return v;
	}
}
