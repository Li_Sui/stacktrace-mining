package ser;
/**
 * This is test class for {@SerialisationTest} class
 * 
 * Hierarchy 
 * 
 *  		  cl1
 * 			   |
 *            cl2
 *            / \
 *          cl3 cl4
 *          
 *          
 * @author Li Sui
 *
 */
import java.io.Serializable;

public class Cl3 extends Cl2 implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6409164009616871844L;

	public void foo(){
		System.out.println("this is cl3");
	}
}
