package ser;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
/**
 * 
 * Currently, wala generates a huge callgraph if we include serialise() method. Manually added the serialised object in the root directory.
 * 
 * @author Li Sui
 *
 */
public class SerialisationTest {

	 public static void main (String args[]) {
		

		 // serialise();
		 deserialise();
	 }
	 
//	 public static void serialise(){
//		Cl3 c3=new Cl3();
//		   try{
//
//			FileOutputStream fout = new FileOutputStream("ser.ser");
//			ObjectOutputStream oos = new ObjectOutputStream(fout);
//			oos.writeObject(c3);
//			oos.close();
//			System.out.println("Done");
//
//		   }catch(Exception ex){
//			   ex.printStackTrace();
//		   }
//	 }
	 
	 public static void deserialise(){
		 Cl1 ref;
		  try{

			   FileInputStream fin = new FileInputStream("src/main/resources/ser.ser");
			   ObjectInputStream ois = new ObjectInputStream(fin);
			   ref = (Cl2) ois.readObject();
			   ref.foo();
			   ois.close();

			  
		   }catch(Exception ex){
			   ex.printStackTrace();
			   
		   }
	 }
	 
	 
	 

	 
}
