package ser;
/**
 * This is test class for {@SerialisationTest} class
 * 
 * Hierarchy 
 * 
 *  		  cl1
 * 			   |
 *            cl2
 *            / \
 *          cl3 cl4
 *          
 *          
 * @author Li Sui
 *
 */
public class Cl2 extends Cl1{
	
	public void foo(){
		System.out.println("this is cl2");
	}

}
