package dp;

import java.lang.reflect.*;
/**
 * 
 * This is an example using java.lang.reflect.Proxy
 * 
 * @author Li Sui
 *
 */
public class DynamicProxyTest {
public static void main(String... args)throws ClassNotFoundException, InstantiationException, IllegalAccessException  {
   
   DynamicProxyTest dp =new DynamicProxyTest();
   TestIF t = (TestIF) Proxy.newProxyInstance(TestIF.class.getClassLoader(),
                           new Class<?>[] {TestIF.class},
                           dp.new TestInvocationHandler());

   t.foo();
}

	public interface TestIF {
		 void foo();
	}
	
	public class TestImple implements TestIF{
	
		@Override
		public void foo() {
		}
		
	}

	public class TestInvocationHandler implements InvocationHandler {
	
		@Override
		public Object invoke(Object obj, Method m, Object[] arg) 
		        throws Throwable {	
			
		   return m.invoke(new TestImple (), arg);
		}
	}

}

