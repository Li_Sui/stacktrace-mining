package nz.ac.massey.wala.callgraph;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisOptions.ReflectionOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilderCancelException;
import com.ibm.wala.ipa.callgraph.CallGraphStats;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.Predicate;
import com.ibm.wala.util.collections.CollectionFilter;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.GraphSlicer;
import com.ibm.wala.util.io.FileProvider;
public class Main {
	
	
	public static void main(String[] args) throws Exception {
		List<String> example =new ArrayList<>();
		String arg=args[0].toLowerCase();
		
		
		boolean whole_callgraph_mode=false;
		if(args[1].equals("true") || args[1].equals("on")){
			whole_callgraph_mode=true;
		}
		
		if(arg.equals("${example}")){
			System.out.println("please specify an input benchmarks. e.g. -Dexample=basic1");
			
		}else{
			
			switch(arg){
				case "reflection1":
					example.add("Lrefl/Reflection1");
					break;
				case "reflection2":
                    example.add("Lrefl/Reflection2");
					break;
				case "reflection3":
                    example.add("Lrefl/Reflection3");
					break;
				case "reflection4":
                    example.add("Lrefl/Reflection4");
					break;
				case "reflection5":
                    example.add("Lrefl/Reflection5");
					break;
				case "reflection6":
					example.add("Lrefl/Reflection6");
					break;
				case "reflection7":
					example.add("Lrefl/Reflection7");
					break;
				case "reflection8":
                    example.add("Lrefl/Reflection8");
					break;
				case "reflection9":
                    example.add("Lrefl/Reflection9");
					break;
				case "basic1":
                    example.add("Lbasic/Basic1");
					break;
				case "basic2":
                    example.add("Lbasic/Basic2");
					break;
				case "basic3":
                    example.add("Lbasic/Basic3");
					break;
				case "basic4":
                    example.add("Lbasic/Basic4");
					break;
				case "lambda":
                    example.add("LinvokeDYN/LambdaTest");
					break;
				case "dynamicproxy":
                    example.add("Ldp/DynamicProxyTest");
					break;
				case "dynamo":
                    example.add("LinvokeDYN/client/DynamoClient");
					break;
				case "serialisation":
                    example.add("Lser/SerialisationTest");
					break;
				case "serviceloader":
                    example.add("LservLoader/ServiceLoaderTest");
				case "dacapo":
                    example=WalaUtil.collectEntryPoints();
					
			}
			System.out.println("--------------------------------------------------------------------------------------------");
			System.out.println("generating whole callgraph mode:"+ whole_callgraph_mode+" ...............................................");
			System.out.println("--------------------------------------------------------------------------------------------");

			AnalysisScope scope = AnalysisScopeReader.makeJavaBinaryAnalysisScope("build/app.jar", (new FileProvider()).getFile("src/main/resources/Java60RegressionExclusions.txt"));
			
			ClassHierarchy cha = ClassHierarchy.make(scope); 
			Iterable<Entrypoint> entrypoints =com.ibm.wala.ipa.callgraph.impl.Util.makeMainEntrypoints(scope, cha);
		     
		    AnalysisOptions options = new AnalysisOptions(scope, entrypoints);
		    options.setReflectionOptions(ReflectionOptions.FULL);

		    com.ibm.wala.ipa.callgraph.CallGraphBuilder builder = Util.makeZeroCFABuilder(options, new AnalysisCache(), cha, scope);

		    CallGraph allCG = builder.makeCallGraph(options, null);
           
		    System.err.println(CallGraphStats.getStats(allCG));

		    Graph<CGNode> applicationCG =pruneGraph(allCG, new ApplicationLoaderFilter());
//		    TypeReference tr = TypeReference.findOrCreate(ClassLoaderReference.Application, "Lbenchmark/servLoader/ServiceImple1");
//		    MethodReference mr = MethodReference.findOrCreate(tr, "foo", "()V");
//		    System.out.println(allCG.getNodes(mr));
		    
		    if(whole_callgraph_mode){
		    	convertToDot(allCG,args[0]);
		    }else{
		    	convertToDot(applicationCG,args[0]);
		    }
			    
		}		    
		 
	}
		public static void convertToDot(Graph<CGNode> g,String testFileName) throws IOException{

            File dotFile =new File("build/wala-output/"+testFileName+"/"+testFileName+".dot");
            File csvFile=new File("build/wala-output/"+testFileName+"/"+testFileName+".csv");
            if (!dotFile.exists() || !csvFile.exists()) {
                dotFile.getParentFile().mkdir();
                dotFile.createNewFile();

                csvFile.getParentFile().mkdir();
                csvFile.createNewFile();
            }


            FileWriter  csvfw =new FileWriter(csvFile.getAbsoluteFile(), true);
            FileWriter  dotfw =new FileWriter(dotFile.getAbsoluteFile(), true);
            // FileWriter  methodName=new FileWriter("build/soot-methods.txt",true);
            dotfw.write("digraph "+testFileName+" {");
//        StringBuffer dotBuffer =new StringBuffer("digraph "+testFileName+" {");
//        StringBuffer csvBuffer =new StringBuffer();
            Iterator<CGNode> iter =g.iterator();
            while(iter.hasNext()){
                CGNode n=iter.next();
                Iterator<CGNode> succIter=g.iterator();
                while(succIter.hasNext()) {
                    CGNode n2 = succIter.next();
                    if (g.hasEdge(n, n2)) {
                        dotfw.write("\"" + n.getMethod().getSignature() + "\"" + "->" + "\"" + n2.getMethod().getSignature()+ "\"" + ";");
                        csvfw.write("\"" +  n.getMethod().getSignature() + "\"" + "," + "\"" + n2.getMethod().getSignature() + "\"" + "\n");
                    }
                }
            }


            dotfw.append("}");

            //  methodName.close();

            dotfw.close();
            csvfw.close();
            //diable it for testing
            if(false) {
                try {
                    String[] c = {"dot", "-Tpng", dotFile.getAbsolutePath(), "-o", "build/walaw-output/" + testFileName + "/" + testFileName + ".png"};
                    Runtime.getRuntime().exec(c);
                } catch (Exception err) {
                    System.out.println("dot process is no available, no png file is generated");
                }
            }
        	
	}
	public static <T> Graph<T> pruneGraph(Graph<T> g, Predicate<T> f) throws WalaException {
	    Collection<T> slice = GraphSlicer.slice(g, f);
	    return GraphSlicer.prune(g, new CollectionFilter<T>(slice));
	}
	
	  private static class ApplicationLoaderFilter extends Predicate<CGNode> {
		   
		    @Override public boolean test(CGNode o) {
		      if (o instanceof CGNode) {
		        CGNode n = (CGNode) o;
		        return n.getMethod().getDeclaringClass().getClassLoader().getReference().equals(ClassLoaderReference.Application);
		      } else if (o instanceof LocalPointerKey) {
		        LocalPointerKey l = (LocalPointerKey) o;
		        return test(l.getNode());
		      } else {
		        return false;
		      }
		    }
	  }

}
