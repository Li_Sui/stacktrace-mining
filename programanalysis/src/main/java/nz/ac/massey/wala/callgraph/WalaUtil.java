package nz.ac.massey.wala.callgraph;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * Created by li on 15/02/17.
 */
public class WalaUtil {


    public static List<String> collectEntryPoints() throws Exception{

        FileInputStream fis = new FileInputStream("src/main/resources/dacapoEntryPoint.tmp");
        ObjectInputStream ois = new ObjectInputStream(fis);
        List<String> entryPoints= (List<String>) ois.readObject();
        ois.close();



        return entryPoints;
    }
}
