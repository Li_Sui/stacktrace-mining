package nz.ac.massey.soot.callgraph;

import soot.MethodOrMethodContext;
import soot.SootMethod;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Targets;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by li on 20/12/16.
 */
public class SootUtil {

    public static void convertToDotAndCSV(CallGraph cg, boolean png, String testFileName ) throws IOException {
        File dotFile =new File("build/soot-output/"+testFileName+"/"+testFileName+".dot");
        File csvFile=new File("build/soot-output/"+testFileName+"/"+testFileName+".csv");
        if (!dotFile.exists() || !csvFile.exists()) {
            dotFile.getParentFile().mkdir();
            dotFile.createNewFile();

            csvFile.getParentFile().mkdir();
            csvFile.createNewFile();
        }


        FileWriter  csvfw =new FileWriter(csvFile.getAbsoluteFile(), true);
        FileWriter  dotfw =new FileWriter(dotFile.getAbsoluteFile(), true);
       // FileWriter  methodName=new FileWriter("build/soot-methods.txt",true);
        dotfw.write("digraph "+testFileName+" {");
//        StringBuffer dotBuffer =new StringBuffer("digraph "+testFileName+" {");
//        StringBuffer csvBuffer =new StringBuffer();
        Iterator<MethodOrMethodContext> methods = cg.sourceMethods();
       // methodName.write(cg.toString()+"\n");
        while (methods.hasNext()) {
            SootMethod src = (SootMethod)methods.next();


            if(!src.isJavaLibraryMethod()){
                Iterator<MethodOrMethodContext> targets = new Targets(cg.edgesOutOf(src));
                while (targets.hasNext()) {
                    SootMethod tgt = (SootMethod)targets.next();
                    dotfw.write("\""+src+"\"" + "->" + "\""+tgt+"\""+";");
                    csvfw.write("\""+src+"\""+","+"\""+tgt+"\""+"\n");

                }
            }
        }

        dotfw.append("}");

      //  methodName.close();

        dotfw.close();
        csvfw.close();

        if(png) {
            try {
                String[] c = {"dot", "-Tpng", dotFile.getAbsolutePath(), "-o", "build/soot-output/" + testFileName + "/" + testFileName + ".png"};
                Runtime.getRuntime().exec(c);
            } catch (Exception err) {
                System.out.println("dot process is no available, no png file is generated");
            }
        }
    }
}
