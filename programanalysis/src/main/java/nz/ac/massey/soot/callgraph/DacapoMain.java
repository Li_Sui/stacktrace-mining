package nz.ac.massey.soot.callgraph;

import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.Transform;
import soot.jimple.toolkits.callgraph.CHATransformer;
import soot.jimple.toolkits.callgraph.CallGraph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * TODO: CLEAN UP
 * Created by li on 20/12/16.
 */
public class DacapoMain {

    static String testFileName;
    static boolean spark_on =false;
    public static void main(String[] args) {
        String arg=args[0].toLowerCase();
        if(args[1].equals("spark")){
            spark_on=true;
        }
        List<String> programList = new ArrayList<String>();
        programList.addAll(Arrays.asList("avrora-cvs-20091224.jar","batik-all.jar","eclipse.jar","fop.jar","h2-1.2.121.jar","jython.jar","luindex.jar",
                "lusearch.jar","pmd-4.2.5.jar","sunflow-0.07.2.jar","tomcat-juli.jar","daytrader.jar","xalan.jar","Harness"

        ));

        if(arg.equals("avrora")){
            testFileName="avrora-cvs-20091224";
        }else if(arg.equals("batik")){
            testFileName="batik-all";
        }else if(arg.equals("eclipse")){
            testFileName="eclipse";
        }else if(arg.equals("fop")){
            testFileName="fop";
        }else if(arg.equals("h2")){
            testFileName="h2-1.2.121";
        }else if(arg.equals("jython")){
            testFileName="jython";
        }else if(arg.equals("luindex")){
            testFileName="luindex";
        }else if(arg.equals("lusearch")){
            testFileName="lusearch";
        }else if(arg.equals("pmd")){
            testFileName="pmd-4.2.5";
        }else if(arg.equals("sunflow")){
            testFileName="sunflow-0.07.2";
        }else if(arg.equals("tomcat")){
            testFileName="tomcat-juli";
        }else if(arg.equals("daytrader")){
            testFileName="daytrader";
        }else if(arg.equals("xalan")){
            testFileName="xalan";
        }else if(arg.equals("driver")) {
            testFileName = "Harness";
        }

        analyse();


    }

    public static  void analyse(){

        List<String> argsList = new ArrayList<String>();
    if(spark_on){
            if(testFileName.equals("Harness")){
                System.out.println("working on Dacapo driver with spark enabled, ...............................................");

                argsList.addAll(Arrays.asList("-w",
                        "-p",
                        "cg.spark",
                        "enabled",
                        "-allow-phantom-refs",
                        "-output-format",
                        "n",
                        "-pp",
                        "-soot-class-path",
                        "build/bin/dacapo",
                        "-include",
                        "org.apache.",
                        "-include",
                        "org.w3c.",
                        "-main-class",
                        testFileName,
                        testFileName

                ));
            }else{

                System.out.println("working on Dacapo "+testFileName+" with spark enabled, ...............................................");

                argsList.addAll(Arrays.asList("-w",
                        "-p",
                        "cg.spark",
                        "enabled",
                        "-allow-phantom-refs",
                        "-output-format",
                        "n",
                        "-pp",
                        "-soot-class-path",
                        "build/bin/dacapo",
                        "-include",
                        "org.apache.",
                        "-include",
                        "org.w3c.",
                        "-process-dir",
                        "build/bin/dacapo/jar/" + testFileName+".jar"

                ));
            }

        }else{
            if(testFileName.equals("Harness")){
                System.out.println("working on dacapo driver, ...............................................");

                argsList.addAll(Arrays.asList("-w",
                        "-allow-phantom-refs",
                        "-output-format",
                        "n",
                        "-pp",
                        "-soot-class-path",
                        "build/bin/dacapo",
                        "-include",
                        "org.apache.",
                        "-include",
                        "org.w3c.",
                        "-main-class",
                        testFileName,
                        testFileName

                ));
            }else{

                System.out.println("working on dacapo "+testFileName+", ...............................................");

                argsList.addAll(Arrays.asList("-w",
                        "-allow-phantom-refs",
                        "-output-format",
                        "n",
                        "-pp",
                        "-soot-class-path",
                        "build/bin/dacapo",
                        "-include",
                        "org.apache.",
                        "-include",
                        "org.w3c.",
                        "-process-dir",
                        "build/bin/dacapo/jar/" +testFileName+".jar"

                ));
            }


        }

        PackManager.v().getPack("wjtp").add(new Transform("wjtp.my", new SceneTransformer() {

            @Override
            protected void internalTransform(String phaseName, Map options) {
                if(!spark_on) {
                    CHATransformer.v().transform();
                }else{
                    testFileName=testFileName+"_spark";
                }

                CallGraph cg = Scene.v().getCallGraph();
                try {
                SootUtil.convertToDotAndCSV(cg,false,testFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }));


        soot.Main.main(argsList.toArray(new String[0]));

    }
}
