package nz.ac.massey.soot.callgraph;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import soot.MethodOrMethodContext;
import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootMethod;
import soot.Transform;
import soot.jimple.toolkits.callgraph.CHATransformer;
import soot.jimple.toolkits.callgraph.CallGraph;
import soot.jimple.toolkits.callgraph.Targets;
import soot.options.Options;

public class Main {

	static String testFileName;
	public static void main(String[] args) {
		String arg=args[0].toLowerCase();
		 testFileName=arg;
		String inputPorgram="";
		if(arg.equals("${example}")){
			inputPorgram="basic.Basic1";//default input
			
		}else{
		    if(arg.equals("basic1")){
				inputPorgram="basic.Basic1";
			}else if(arg.equals("basic2")){
				inputPorgram="basic.Basic2";
			}else if(arg.equals("basic3")){
				inputPorgram="basic.Basic3";
			}else if(arg.equals("basic4")){
				inputPorgram="basic.Basic4";
			}else if(arg.equals("reflection")){
			   inputPorgram="refl.Reflection1";
			}else if(arg.equals("dynamicproxy")){
			   inputPorgram="dp.DynamicProxyTest";
			}else if(arg.equals("serialisation")){
				inputPorgram="ser.SerialisationTest";
			}
		}
		System.out.println("working on "+inputPorgram+", ...............................................");
		   List<String> argsList = new ArrayList<String>();
		   argsList.addAll(Arrays.asList("-w",
				   "-p",
				   "cg.spark",
				   "enabled",
				   "-allow-phantom-refs",
				   "-output-format",
				   "n",
				   "-pp",
				   "-soot-class-path",
				   "build/bin",
				   "-include",
				   "org.apache.",
				   "-include",
				   "org.w3c.",
				   "-main-class",
				   inputPorgram,
				   inputPorgram

		   ));

		   PackManager.v().getPack("wjtp").add(new Transform("wjtp.my", new SceneTransformer() {

			@Override
			protected void internalTransform(String phaseName, Map options) {
				   //CHATransformer.v().transform();
			       CallGraph cg = Scene.v().getCallGraph();

			       try {
					SootUtil.convertToDotAndCSV(cg,true,testFileName);
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

			   
		   }));
	           
	           soot.Main.main(argsList.toArray(new String[0]));
	}

}
