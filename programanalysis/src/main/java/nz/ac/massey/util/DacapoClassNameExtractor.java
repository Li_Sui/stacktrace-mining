package nz.ac.massey.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by li on 15/02/17.
 */
public class DacapoClassNameExtractor {
    public static void main(String[] args) throws Exception{
        List<String> classNames = new ArrayList<>();
        try(Stream<Path> paths = Files.walk(Paths.get("build/bin"))) {

            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {

                        if (filePath.toString().endsWith(".class") && !filePath.getFileName().toString().contains("$")) {
                            String className = filePath.toString().substring(10); // including ".class"
                            classNames.add("L"+className.substring(0, className.length() - ".class".length()));
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        FileOutputStream fos = new FileOutputStream("res/dacapoEntryPoint.tmp");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(classNames);
        oos.close();


        FileInputStream fis = new FileInputStream("res/dacapoEntryPoint.tmp");
        ObjectInputStream ois = new ObjectInputStream(fis);
        List<String> clubs = (List<String>) ois.readObject();
        ois.close();

        System.out.println(clubs);
    }
}
