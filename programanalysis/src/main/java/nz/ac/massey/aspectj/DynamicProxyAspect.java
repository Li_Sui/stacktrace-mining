package nz.ac.massey.aspectj;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class DynamicProxyAspect {
	@Before("call (* *(..)) && (within(nz.ac.massey.aspectj.DynamicProxyTest))")
	public void tracedCall(JoinPoint joinPoint){
		Signature sig = joinPoint.getSignature();
		String line = ""
		+ joinPoint.getSourceLocation().getLine();
		
		String sourceName = joinPoint.getSourceLocation()
		.getWithinType().getCanonicalName();
		//
		System.out.println("Call from " + sourceName + " line " + line + "\n   to "
		+ sig.getDeclaringTypeName() + "." + sig.getName() +"\n");
	}
}

