package nz.ac.massey.aspectj;

public class LambdaTest {

	public static void main(String[] args) {	 
		Operation op=(int a, int b)-> sumup(otherOperation(a),b);
		System.out.println(op.operation(2, 2));
	}
	
	public static int otherOperation(int a){
		return a*a;
	}
	
	public static int sumup(int a, int b){
		return a+b;
	}
	interface Operation{
		int operation(int a, int b);
	}
}

