package nz.ac.massey.aspectj;

import java.lang.reflect.*;
public class DynamicProxyTest {
public static void main(String... args)throws ClassNotFoundException, InstantiationException, IllegalAccessException  {
   
   DynamicProxyTest dp =new DynamicProxyTest();
   TestIF t = (TestIF) Proxy.newProxyInstance(TestIF.class.getClassLoader(),
                           new Class<?>[] {TestIF.class},
                           dp.new TestInvocationHandler(dp.new TestImpl()));

   t.hello("Duke");
}

public interface TestIF {
	 String hello(String name);
}
public class TestImpl implements TestIF {
	public String hello(String name) {
          
	   return hi();
	}
        public String hi(){
		return "greeting";
	}
}


public class TestInvocationHandler implements InvocationHandler {
private Object testImpl;
 
public TestInvocationHandler(Object impl) {
   this.testImpl = impl;
}
 
@Override
public Object invoke(Object proxy, Method method, Object[] args) 
        throws Throwable {	
   
   return method.invoke(testImpl, args);
}
}

}

