package nz.ac.massey.aspectj;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTest {

	
	public static  void go() throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		Object v1=new Object();
		Method m = Class.forName("nz.ac.massey.aspectj.ReflectionTest").getDeclaredMethod("id", new Class[]{java.lang.Object.class});
		Object v2=m.invoke(ReflectionTest.class, new Object[]{v1});
		System.out.println(v2);
	}
	
	public static Object id(Object v){
		return v;
	}
	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		go();
	}
}
