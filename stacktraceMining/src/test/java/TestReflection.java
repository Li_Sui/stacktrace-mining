import java.lang.reflect.Method;

public class TestReflection {

    public static void main(String[] args) throws Exception {
        Method m = Class.forName("TestReflection").getDeclaredMethod("id", new Class[]{java.lang.Object.class});
        m.invoke(new TestReflection(), new Object[]{new Object()});
    }

    public  Object id(Object v){
        foo(v);
        return v;
    }

    public Object foo(Object v){
        throw new RuntimeException();
    }
}
