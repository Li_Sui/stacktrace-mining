import java.util.ArrayList;
import java.util.List;

public class TestInvokeDynamic {

    public static void main(String args[]) {
//        Operation op=(int a, int b)-> sumup(otherOperation(a),b);
//       op.operation(2, 2);
//
        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");
        items.add("C");
        items.add("D");
        items.add("E");

        items.forEach(item-> {
            foo();
        } );

    }


    public static void foo(){
        throw new RuntimeException("lol");
    }
    public static int otherOperation(int a){
        return a*a;
    }

    public static void sumup(int a, int b){
        throw new RuntimeException("lol");
    }
    interface Operation{
       void operation(int a, int b);
    }
}
