

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.lang.reflect.*;

public class TestDynamicProxy {

    public static void main(String... args)throws Exception {

        TestDynamicProxy dp =new TestDynamicProxy();
        dp.doit(dp);
    }


    public  void doit(TestDynamicProxy dp) throws Exception{


        TestIF t = (TestIF) Proxy.newProxyInstance(TestIF.class.getClassLoader(),
                new Class<?>[] {TestIF.class},
                dp.new TestInvocationHandler());



        t.foo();
    }

    public interface TestIF {
        void foo() throws IOException;
    }


    public class TestImple implements TestIF{

        @Override
        public void foo() throws IOException {
            bar();

        }

        public void bar() throws IOException{
            throw new IOException();
        }


    }

    public class TestInvocationHandler implements InvocationHandler {

        @Override
        public Object invoke(Object obj, Method m, Object[] arg) throws Exception {
            //throw new Exception("bad");

            return m.invoke(new TestImple (), arg);
        }


    }
}
