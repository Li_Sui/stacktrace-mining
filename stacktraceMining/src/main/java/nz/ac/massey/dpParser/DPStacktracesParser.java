package nz.ac.massey.dpParser;

import nz.ac.massey.mining.ParserUtil;
import nz.ac.massey.mining.StackTraceElement;
import nz.ac.massey.mining.Stacktrace;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 *
 * Identify and parse stacktraces for Dynamic proxy
 * Created by li on 12/01/18.
 */
public class DPStacktracesParser {

    private String stack_element_regex = "at\\s((\\p{Alpha}|[0-9])\\.)*(\\p{Alnum}|\\_|\\$|\\>|\\<|\\.)+\\((\\p{Alnum}|\\.|\\:|\\p{Space}|\\[|\\])+\\)";
    private String pre_signaler_package = "\\p{Alnum}(\\.|\\p{Alnum}|\\_|\\>|\\$|\\<|\\p{Space})+\\(";
    private String lineNo_regex="\\.java\\:(([0-9]+))";
    private String target_exception_regex="java.lang.reflect.UndeclaredThrowableException";
    private String reflectiveCall="java.lang.reflect.InvocationTargetException";
    private String causeByStatement="Caused by:";

    private ParserUtil util=new ParserUtil(stack_element_regex,pre_signaler_package,lineNo_regex);




    public void parseDirectory(String dir, String outputDir) throws Exception{



        try(Stream<Path> paths = Files.walk(Paths.get(dir))) {
            Map<String,String> reflectiveCalls=new HashMap<>();
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        String content=FileUtils.readFileToString(filePath.toFile());
                        content.replaceAll("(\\r|\\n)", "");//remove all space and line

                        List<List<Stacktrace>> sts=extractStacktraces(content);


                        findandSave(sts, filePath.getFileName().toString(),outputDir,reflectiveCalls);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });





//            //save to one file:
            StringBuilder builder = new StringBuilder();
            Iterator it = reflectiveCalls.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                builder.append(pair.getKey().toString()+pair.getValue().toString());


            }


            PrintWriter out = new PrintWriter( outputDir+"/DynamicProxy.csv");
            out.print(builder.toString());
            out.close();
            System.out.println("size:" +reflectiveCalls.size());

        }

        //System.out.println(extractElements(readContent("build/test3.txt")));

    }


    /**
     * extrace stacktrace from text
     * @param content
     * @return
     */
     private List<List<Stacktrace>>  extractStacktraces(String content){
        List<List<Stacktrace>> list =new ArrayList<>();
         String[] parts=content.split(target_exception_regex);

         for(int i=1;i<parts.length;i++){
             List<Stacktrace> p =new ArrayList<>();
             String[] causeBy=parts[i].split(causeByStatement);
             for(String block:Arrays.asList(causeBy)){

                 List<StackTraceElement> elements=util.extractElements(block);

                 if(elements.size()!=0){
                     Stacktrace st=new Stacktrace();
                     st.setElements(elements);

                     p.add(st);
                 }


             }
             list.add(p);
         }
         return list;
     }




    private void findandSave( List<List<Stacktrace>> sts,String fileName,String outputDir,Map<String,String> reflectiveCallList) throws Exception{

        StringBuilder pairs=new StringBuilder("");

        for(int j=0;j<sts.size();j++){
            List<Stacktrace> st=sts.get(j);
            String source="";
            int srcLine=0;
            String target="";
            int targetLine=0;


            if(st.size()>=3) {
                Stacktrace mainException = st.get(0);
                if(mainException.getElements().size()>=2){
                    if(mainException.getElements().get(0).getMethodName().contains("$Proxy")){
                        source=mainException.getElements().get(1).getMethodName();
                        srcLine=mainException.getElements().get(1).getLineNumber();
                    }else{
                        source=mainException.getElements().get(0).getMethodName();
                        srcLine=mainException.getElements().get(0).getLineNumber();
                    }
                }

                Stacktrace targetBlock=st.get(2);

                if(!targetBlock.getElements().isEmpty()){
                    target=targetBlock.getElements().get(targetBlock.getElements().size()-1).getMethodName();
                    targetLine=targetBlock.getElements().get(targetBlock.getElements().size()-1).getLineNumber();
                }

               System.out.println("found at: "+ URLDecoder.decode(fileName,"UTF-8")+"         \"" + source + "\",\"" + target + "\"\n");
                if(!source.isEmpty()) {
                    reflectiveCallList.put("\"" + source + "\",\"" + srcLine + "\",\"" + target + "\", \"" + targetLine + "\"", ",\"" + URLDecoder.decode(FilenameUtils.removeExtension(fileName), "UTF-8") + "\"\n");
                }


            }


        }

    }



}
