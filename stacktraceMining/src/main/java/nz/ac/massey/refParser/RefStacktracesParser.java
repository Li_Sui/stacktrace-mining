package nz.ac.massey.refParser;

import nz.ac.massey.mining.ParserUtil;
import nz.ac.massey.mining.StackTraceElement;
import nz.ac.massey.mining.Stacktrace;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 *
 * Identify and parse stacktraces for reflection
 * Created by li on 12/01/17.
 */
public class RefStacktracesParser {

    private String stack_element_regex = "(([a-z]+)\\.)(\\p{Alpha}|[0-9])+\\.(\\p{Alnum}|\\_|\\$|\\>|\\<|\\.)+\\((\\p{Alnum}|\\.|\\:|\\p{Space}|\\[|\\])+\\)";
    private String pre_signaler_package = "\\p{Alnum}(\\.|\\p{Alnum}|\\_|\\>|\\$|\\<|\\p{Space})+\\(";
    private String lineNo_regex="\\.java\\:(([0-9]+))";
    private String target_exception_regex="java.lang.reflect.InvocationTargetException";
    private ParserUtil util=new ParserUtil(stack_element_regex,pre_signaler_package,lineNo_regex);

    public void parseDirectory(String dir, String outputDir) throws Exception{



        try(Stream<Path> paths = Files.walk(Paths.get(dir))) {
            Map<String,String> reflectiveCalls=new HashMap<>();
            paths.forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        String content=FileUtils.readFileToString(filePath.toFile());
                        content.replaceAll("(\\r|\\n)", "");
                        List<Stacktrace> sts =extractStacktraces(content);

                        findandSave(sts, filePath.getFileName().toString(),outputDir,reflectiveCalls);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });



//            //save to one file:
            StringBuilder builder = new StringBuilder();
            Iterator it = reflectiveCalls.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                builder.append(pair.getKey().toString()+pair.getValue().toString());


            }


            PrintWriter out = new PrintWriter( outputDir+"/AllReflectiveCalls.csv");
            out.print(builder.toString());
            out.close();
            System.out.println("size:" +reflectiveCalls.size());

        }

        //System.out.println(extractElements(readContent("build/test3.txt")));

    }


    /**
     * extrace stacktrace from text
     * @param content
     * @return
     */
    private List<Stacktrace>  extractStacktraces(String content){
        // split the doc into serveral parts, based on "InvocationTargetException"
        String[] s1=content.split(target_exception_regex);
        List<Stacktrace> stacktraces=new ArrayList<>();
        for(int i=1;i<s1.length;i++){//always ignore the text before InvocationTargetException
            //split again based on "Caused by:"
            String[] s2=s1[i].split("Caused by: ");


            for(String t:Arrays.asList(s2)){
                List<StackTraceElement> elements=util.extractElements(t);

                if(elements.size()!=0){
                    Stacktrace st=new Stacktrace();
                    st.setElements(elements);

                    stacktraces.add(st);
                }
            }

        }
        return stacktraces;
    }



    private void findandSave( List<Stacktrace> sts,String fileName,String outputDir,Map<String,String> reflectiveCallList) throws Exception{
        boolean flag=false;
        StringBuilder pairs=new StringBuilder("");

        for(int j=0;j<sts.size();j++){
            Stacktrace st=sts.get(j);
            String source="";
            int srcLine=0;

            for(int i=0;i<st.getElements().size();i++){
                StackTraceElement element=st.getElements().get(i);

                if(element.getMethodName().equals("java.lang.reflect.Method.invoke")) {

                    if (i + 1 < st.getElements().size() ) {
                        if(st.getElements().get(i + 1).getMethodName().equals("java.lang.reflect.Method.invoke") && i + 2 < st.getElements().size()){
                            source=st.getElements().get(i + 2).getMethodName();
                            srcLine=st.getElements().get(i+2).getLineNumber();
                            flag = true;

                        }else{
                            source=st.getElements().get(i + 1).getMethodName();
                            srcLine=st.getElements().get(i+1).getLineNumber();
                            flag = true;
                        }
                    }
                    break;
                }
            }
            if(flag) {
                if (j + 1 < sts.size()) {

                    String target=sts.get(j + 1).getElements().get(sts.get(j + 1).getElements().size() - 1).getMethodName();
                    int targetLine=sts.get(j + 1).getElements().get(sts.get(j + 1).getElements().size() - 1).getLineNumber();

                    // if(checkDacapoDataset(source,target)) {
                    //System.out.println("found at: "+ URLDecoder.decode(fileName,"UTF-8")+"         \"" + source + "\",\"" + target + "\"\n");
                    reflectiveCallList.put("\"" + source + "\",\""+srcLine+"\",\"" + target + "\", \""+targetLine+"\"",",\""+URLDecoder.decode(FilenameUtils.removeExtension(fileName),"UTF-8")+"\"\n");

                    // }
                }
                flag = false;
            }
        }

        //System.out.println(paris);

//        if(paris.length()!=0){
//            PrintWriter out = new PrintWriter( outputDir+"/"+ FilenameUtils.removeExtension(fileName) + ".csv");
//            out.print(paris);
//            out.close();
//
//        }
    }



}