package nz.ac.massey.mining;

import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;


/**
 * Created by li on 31/01/17.
 */
public class EdgesFinder {

    static String callgraphIDataset="build/antlr-methodName";
    static String refectiveCallsDataset="build/AllReflectiveCalls.csv";

    public static void main(String[] args) throws Exception{

            File f =new File(refectiveCallsDataset);
            BufferedReader br=new BufferedReader(new FileReader(f));
            String line;
            while((line=br.readLine())!=null){
                String[] methods=line.split(",");
                if(checkDataset( methods[0],methods[1])) {
                    System.out.println("Found at :" + URLDecoder.decode(methods[2], "UTF-8"));
                    System.out.println("---------------------------------------------------------------------------------");
                }
            }

    }

    public static boolean checkDataset(String source, String target) throws Exception{

        Iterator<File> it = FileUtils.iterateFiles(new File(callgraphIDataset), new String[]{"csv"}, true);
        while(it.hasNext()) {
            File f = it.next();
            BufferedReader br=new BufferedReader(new FileReader(f));
            String line;
            while((line=br.readLine())!=null){
                String[] methods=line.split(",");
                if(methods[0].equals(source) && !methods[1].equals(target) && FileUtils.readFileToString(f).contains(target)){
                    System.out.println("Found in the repository dataset: source: "+source+"  target: "+target);
                    System.out.println("meanwhile in the callgraph dataset: source: "+methods[0]+"  target: "+methods[1]);

                    return true;
                }
            }
        }
        return false;

    }
}
