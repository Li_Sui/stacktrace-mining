package nz.ac.massey.mining;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;

/**
 * Not been used !!!!!!
 * Use Google Custom Search Engine to retieve results and strip off html elements.
 * Created by li on 23/12/16.
 */
public class GoogleCS {

   // static int startIndex=1;
    static String key="006308722713195660550:_blwcf3tswe";
    static String qry="java.lang.reflect.InvocationTargetException";
    static String APIkey="AIzaSyC4l0a-Fawv4hg1IrBoybXbOMYfsKThYaM";
    static int counter=0;
    public static void main(String[] args)throws Exception{
        StringBuilder links =new StringBuilder();
        int iterations=20;
        int startIndex=1;
        for(int i=0;i<iterations;i++){

            sendReqt(links,startIndex);
            startIndex=startIndex+10;
            System.out.println("Output from Server ,retrieved "+startIndex+" results.... \n");
        }
    }

    public static void sendReqt(StringBuilder links,int startIndex)throws Exception{
        URL url = new URL(
                "https://www.googleapis.com/customsearch/v1?key="+APIkey+ "&cx="+key+"&q="+ qry + "&alt=json&start="+startIndex);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;
        StringBuilder json =new StringBuilder();

        while ((output = br.readLine()) != null) {

            json.append(output);

            if(output.contains("\"link\": \"")){
                String link =output.substring(output.indexOf("\"link\": \"")+("\"link\": \"").length(), output.indexOf("\","));
                links.append(link+"\n");
                //stripHTMLElements(link);
                counter++;

            }
        }
        br.close();

        conn.disconnect();

        //JSONObject obj = new JSONObject(json.toString());

       // startIndex=((JSONObject) obj.getJSONObject("queries").getJSONArray("nextPage").get(0)).getInt("startIndex");//get the index(page number)

    }

    public static void stripHTMLElements(String link)throws Exception{
        Document doc = Jsoup.connect(link).get();


        //save to separated files
        BufferedWriter bw=new BufferedWriter(new FileWriter("build/"+counter+".txt"));
        bw.write(doc.body().text());
        bw.close();
    }
}
