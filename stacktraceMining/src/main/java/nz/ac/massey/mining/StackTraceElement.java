package nz.ac.massey.mining;

import java.io.Serializable;

/**
 * Created by li on 19/01/17.
 */
public class StackTraceElement implements Serializable {

    private String className;
    private String methodName;
    private int lineNo;

    public StackTraceElement(String className,String methodName,int lineNo){

        this.className=className;
        this.methodName=methodName;
        this.lineNo=lineNo;
    }


    public String getClassName() {
        return className;
    }

    public String getMethodName() {
        return methodName;
    }

    public int getLineNumber(){return lineNo;}

    @Override
    public String toString() {
        return "StackTraceElement{" +
                "className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", lineNumber='" +lineNo +'\''+
                '}';
    }
}
