package nz.ac.massey.mining;

import java.io.Serializable;
import java.util.List;

/**
 *
 * Stacktrace structure
 * Created by li on 13/01/17.
 */
public class Stacktrace implements Serializable {

    private List<StackTraceElement> elements;
    private String throwable;


    public Stacktrace(){
        //do nothing
    }

    public void setElements(List<StackTraceElement> elements) {
        this.elements = elements;
    }


    public void setThrowable(String throwable) {
        this.throwable = throwable;
    }

    public List<StackTraceElement> getElements() {
        return elements;
    }


    public String getThrowable() {
        return throwable;
    }

    @Override
    public String toString() {
        return "Stacktrace{" +
                "elements=" + elements +
                '}';
    }
}
