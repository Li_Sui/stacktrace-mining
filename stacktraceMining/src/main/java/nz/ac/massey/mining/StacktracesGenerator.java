package nz.ac.massey.mining;

import nz.ac.massey.dpParser.DPStacktracesParser;
import nz.ac.massey.refParser.RefStacktracesParser;

/**
 * Created by li on 30/01/17.
 */
public class StacktracesGenerator {

    static String intpuDir="build/mining";
    static String outputDir="build";
    public static void main(String[] args){

        if(args[0].equals("dynamicproxy")){
            DPStacktracesParser dpp=new DPStacktracesParser();
            try {
                dpp.parseDirectory(intpuDir,outputDir);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if(args[1].equals("reflection")){
            RefStacktracesParser refp=new RefStacktracesParser();
            try {
                refp.parseDirectory(intpuDir,outputDir);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }



    }




}
