package nz.ac.massey.mining;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 *
 * Identify and parse stacktraces
 * Created by li on 12/01/17.
 */
public class ParserUtil {

    private String stack_element_regex;
    private String pre_signaler_package;
    private String lineNo_regex;

    public ParserUtil(String stack_element_regex,String pre_signaler_package,String lineNo_regex){
        this.stack_element_regex=stack_element_regex;
        this.pre_signaler_package=pre_signaler_package;
        this.lineNo_regex=lineNo_regex;
    }

    /**
     * Not being used, test purpose.
     * @param filename
     * @return
     * @throws Exception
     */
    protected String readContent(String filename)throws Exception{
        FileInputStream fstream = new FileInputStream(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        StringBuilder content=new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            content.append(line);
        }
        return content.toString();
    }



    /**
     * extract stacktrace element
     * @param stackTrace
     * @return
     */
    public ArrayList<StackTraceElement> extractElements(String stackTrace) {
        Pattern pattern = Pattern.compile(stack_element_regex);
        ArrayList<StackTraceElement> elements = new ArrayList<StackTraceElement>();
        if (stackTrace != null && !stackTrace.isEmpty()) {
            Matcher matcher = pattern.matcher(stackTrace);
            while (matcher.find()) {

                String exceptionPattern = matcher.group();
                String className=getClassName(exceptionPattern);
                String methodName=getMethodName(exceptionPattern);
                int lineNo=getLineNumber(exceptionPattern);

                elements.add(new StackTraceElement(className,methodName,lineNo));
            }
        }
        return elements;
    }

    private int getLineNumber(String name){
        Pattern pattern = Pattern.compile(lineNo_regex);
        Matcher matcher;
        int lineNumber=0;
        if (name != null && !name.isEmpty() && (matcher = pattern.matcher(name)).find()) {

            lineNumber= Integer.valueOf(matcher.group().replaceAll("\\D+",""));
        }
        return lineNumber;
    }
    private String getClassName(String name) {
        Matcher matcher;
        int index;
        Pattern pattern = Pattern.compile(pre_signaler_package);
        String sig_class = null;
        if (name != null && !name.isEmpty() && (matcher = pattern.matcher(name)).find() && (index = (sig_class = matcher.group()).indexOf(40)) != -1) {
            sig_class = sig_class.substring(0, index);
            index = sig_class.lastIndexOf(46);
            if (index != -1) {
                sig_class = sig_class.substring(0, index);
                index = sig_class.lastIndexOf(32);
                if (index != -1) {
                    sig_class = sig_class.substring(index, sig_class.length());
                    sig_class = sig_class.trim();
                }
            }
        }
        return sig_class;
    }

    private String getMethodName(String name) {
        Matcher matcher;
        int index;
        Pattern pattern = Pattern.compile(pre_signaler_package);
        String sig_met = null;
        if (name != null && !name.isEmpty() && (matcher = pattern.matcher(name)).find() && (index = (sig_met = matcher.group()).indexOf(40)) != -1) {
            sig_met = sig_met.substring(0, index);
            index = sig_met.lastIndexOf(32);
            if (index != -1) {
                sig_met = sig_met.substring(index, sig_met.length());
                sig_met = sig_met.trim();
            }
        }
        return sig_met;
    }



}
