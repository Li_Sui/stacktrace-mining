package nz.ac.massey.mining;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 * Created by li on 25/01/17.
 */
public class SootOutputMethodNameConverter {

    static String dacapoDir="build/soot-output";
    static String outputDir="build/hadoop-hbase-methodName";


    public static void main(String[] args) throws Exception{

        Iterator<File> it = FileUtils.iterateFiles(new File(dacapoDir), new String[]{"csv"}, true);
        String pattern = "(<.*?:)|(\\s(\\p{Alnum}|\\_|\\$|\\>|\\<)+\\()";
        Pattern r = Pattern.compile(pattern);

        while(it.hasNext()){

            File f=it.next();

            BufferedReader br=new BufferedReader(new FileReader(f));
            StringBuilder temp=new StringBuilder("");
            String line;
            while((line=br.readLine())!=null){
                String[] ones=line.split("\",\"");
                temp.append("\""+convertMethodName(ones[0],r)+"\"");
                temp.append(",");
                temp.append("\""+convertMethodName(ones[1],r)+"\"\n");
            }

            PrintWriter out =new PrintWriter(outputDir+"/"+f.getName());
            out.print(temp);
            out.close();

        }

    }

    public static String convertMethodName(String input, Pattern r){
        Matcher m = r.matcher(input);
        StringBuilder b=new StringBuilder();
        while(m.find() ){
            String name=m.group();
            b.append("."+name.substring(1,name.length()-1));
        }
        return b.substring(1);
    }

}
