package nz.ac.massey.mining;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.net.URLEncoder;
import java.util.*;

/**
 * Mining statckoverflow and github
 * Created by li on 23/01/17.
 * updated on 26/02/18
 */
public class MiningRepositories {

    private Map<String,String> cookies=new HashMap<String,String>();

    private String userAgent;

    private String targetException="java.lang.reflect.UndeclaredThrowableException";

    private static int stackoverflowTotalPages=16;
    private static int githubTotalPages=73;



    public MiningRepositories(Map<String,String> cookie,String userAgent){
        this.cookies=cookie;

        this.userAgent=userAgent;

    }
    public void crawlingGithub(int pagenum)throws Exception{

        Map<String,String> query=new HashMap<String,String>();
        query.put("p",Integer.toString(pagenum));
        query.put("q",targetException);
        query.put("type","Issues");
        query.put("utf8","%E2%9C%93");
        Document doc =Jsoup.connect("https://github.com/search")
                .userAgent(userAgent)
                .cookies(cookies).data(query).get();


        for(int i=0;i<doc.getElementsByClass("issue-list-item col-12 py-4").size();i++){
            System.out.println("--------------working on the "+(i+1)+" entry");
            String link="https://github.com"+doc.getElementsByClass("issue-list-item col-12 py-4").get(i).select("h3.text-normal.pb-1 > a").attr("href");
            System.out.println(link);
            try {
                Random r=new Random();
                Document d = Jsoup.connect(link).userAgent(userAgent).get();

                BufferedWriter bw=new BufferedWriter(new FileWriter("build/github/"+ URLEncoder.encode(link, "UTF-8")+".txt"));
                bw.write(d.body().text());
                bw.close();
//                int sleep=r.nextInt(2)+1;
//                System.out.println("done! wait for "+sleep+" seconds");
//                Thread.sleep(sleep*1000);
            } catch (HttpStatusException e){
                System.err.print(e);
            }

        }







    }
    public void crawlingStackoverflow(int pagenum)throws Exception{
        Map<String,String> query=new HashMap<String,String>();
        query.put("page",Integer.toString(pagenum));
        query.put("tab","relevance");
        query.put("q",targetException);
        Document doc = Jsoup.connect("http://stackoverflow.com/search")
                .userAgent(userAgent)
                .cookies(cookies).data(query).get();




        for(int i=0;i<doc.getElementsByClass("result-link").size();i++){
            System.out.println("--------------working on the "+(i+1)+" entry");
            String link="http://stackoverflow.com"+doc.getElementsByClass("result-link").get(i).select("span > a").attr("href");
            System.out.println(link);
            try {
                Random r=new Random();
                Document d = Jsoup.connect(link).get();
                // System.out.println(d.body().text());
                BufferedWriter bw=new BufferedWriter(new FileWriter("build/stackoverflow/"+URLEncoder.encode(link, "UTF-8")+".txt"));
                bw.write(d.body().text());
                bw.close();
                int sleep=r.nextInt(2)+1;
                System.out.println("done! wait for "+sleep+" seconds");
                Thread.sleep(sleep*1000);
            } catch (Exception e){
                System.err.print(e);
            }


        }
    }

    public static void main(String[] args) throws Exception {

        String userAgent ="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36";
        Map<String,String> githubcookies=new HashMap<String,String>();
        githubcookies.put("_octo","GH1.1.840979469.1518655458");
        githubcookies.put("_gid","GA1.2.492717981.1519353760");
        githubcookies.put("logged_in","yes");
        githubcookies.put("dotcom_user","happylee");
        githubcookies.put(" _gh_sess", "eyJzZXNzaW9uX2lkIjoiYzI4YmMyYzQwMTk3NTVmZDhkMDM0OTk3MzU3MDI1ODIiLCJsYXN0X3JlYWRfZnJvbV9yZXBsaWNhcyI6MTUxOTM1NzM2OTc5NCwiY29udGV4dCI6Ii8iLCJzcHlfcmVwbyI6Imh5YjE5OTYtZ3Vlc3QvYXV0by5qczMtaXNzdWVzIiwic3B5X3JlcG9fYXQiOjE1MTkzNTczNjl9--3493d0ad8726c728d1bc0f868f385a44cf0ab11c");
        githubcookies.put("user_session","45iwYgFiGxNm5j2CLEGPnQzfs-2QOGuoqvETqO3pjue8DGo1");
        githubcookies.put("__Host-user_session_same_site","45iwYgFiGxNm5j2CLEGPnQzfs-2QOGuoqvETqO3pjue8DGo1");
        githubcookies.put("_ga","GA1.2.75571213.1518655458");
        githubcookies.put("tz","Pacific%2FAuckland");
        MiningRepositories miningGithub = new MiningRepositories(githubcookies,userAgent);
        for(int i=1;i<githubTotalPages+1;i++){
            System.out.println("working on page "+i);
            miningGithub.crawlingGithub(i);

        }


        Map<String,String> stackoverflowcookies=new HashMap<String,String>();
        stackoverflowcookies.put("prov","fc7cae74-cd4a-0e0e-dbfa-46267e3600d3");
        stackoverflowcookies.put("__qca","P0-80963487-1518571912703");
        //stackoverflowcookies.put("usr","p=[10|50]");
        stackoverflowcookies.put("acct","=t=VpXvJyP8Am%2bbwgxm%2bwVoKIFRwgIACHC6&s=mB%2fqKDRzP9EwTIf3Elmdv6sSw3aoqEAv");
        stackoverflowcookies.put("_gat","1");
        stackoverflowcookies.put("_gat_pageData","1");
        stackoverflowcookies.put("_ga","GA1.2.814124761.1518571913");
        MiningRepositories miningStackoverflow = new MiningRepositories(stackoverflowcookies,userAgent);

        //total paper number
        for(int i=1;i<stackoverflowTotalPages+1;i++){
            System.out.println("working on page "+i);
            miningStackoverflow.crawlingStackoverflow(i);
        }
    }


}
