package nz.ac.massey.versionAnalyser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * read csv into Edge model
 * Created by li on 15/05/17.
 */
public class LineMatcher {

    private String cvsSplitBy = ",";
    private  Map<String,Map<String,List<Integer>>> extractedFromBytecode;

    public LineMatcher( Map<String,Map<String,List<Integer>>> extractedFromBytecode){
        this.extractedFromBytecode=extractedFromBytecode;
    }


    public List<Edge> read(String filePath) throws Exception{
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        List<Edge> edges= new ArrayList<>();
        String line = "";
        while ((line = br.readLine()) != null) {
            String[] parts = line.split(cvsSplitBy);
            //e.g. net.sf.cram.CramTools.invoke,93,net.sf.cram.Cram2Fastq.main,94,https://github.com/vadimzalunin/crammer/issues/80
            edges.add(new Edge(parts[0],Integer.parseInt(parts[1]),parts[2],Integer.parseInt(parts[3]),parts[4]));
        }

        return edges;
    }

    public List<Edge> computeMatches(String fileName) throws Exception{
        List<Edge> edges=edges= read(fileName);
        for(Edge e:edges){
            if(extractedFromBytecode.containsKey(e.getSourceCallsite())){
                for(Map.Entry<String,List<Integer>> lineAndVersion: extractedFromBytecode.get(e.getSourceCallsite()).entrySet()){
                    String version =lineAndVersion.getKey();
                    for(Integer l: lineAndVersion.getValue()){
                        if(e.getSourceCallsiteLineNO()-l==0){
                            e.addVerion(version);
                            break;
                        }
                    }
                }
            }
        }

        return edges;

    }


}
