package nz.ac.massey.versionAnalyser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by li on 15/05/17.
 */
public class Edge {

    private String sourceCallsite;
    private String targetCallsite;
    private int sourceCallsiteLineNO;
    private int targetCallsiteLineNO;
    private String url; //website we extracted from
    private List<String> versions; //all possible version(where score=0)

    public Edge( String sourceCallsite,int sourceCallsiteLineNO,String targetCallsite,int targetCallsiteLineNO,String url){
        this.sourceCallsite=sourceCallsite;
        this.sourceCallsiteLineNO=sourceCallsiteLineNO;
        this.targetCallsite=targetCallsite;
        this.targetCallsiteLineNO=targetCallsiteLineNO;
        this.url=url;
        this.versions=new ArrayList<>();
    }

    public void addVerion(String version){
        this.versions.add(version);
    }

    public String getSourceCallsite() {
        return sourceCallsite;
    }

    public String getTargetCallsite() {
        return targetCallsite;
    }

    public int getSourceCallsiteLineNO() {
        return sourceCallsiteLineNO;
    }

    public int getTargetCallsiteLineNO() {
        return targetCallsiteLineNO;
    }

    public String getUrl() {
        return url;
    }


    public List<String> getVersions() {
        return versions;
    }
}
