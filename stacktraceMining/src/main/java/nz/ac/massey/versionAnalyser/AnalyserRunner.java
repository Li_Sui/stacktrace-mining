package nz.ac.massey.versionAnalyser;

import org.objectweb.asm.ClassReader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 *
 * INPUT: a:path to all binary files with different versions. e.g. data/<project_name>/<versions>
 *        b:List of target edges need to be analysed.
 *        c: reflective callsites extracted from github and stackoverflow. AllReflectiveCalls.csv
 *
 *  method:
 * 1. compute line numbers in all binary files with different versions based on given list of callsite.
 * 2. find matches. (linenumber from statcktraces - linenumber from bytecode.)
 * 3. produce output.csv
 * Created by li on 11/05/17.
 */
public class AnalyserRunner {
    //path to where the projects are. The structure must be :data/<project_name>/<versions>
    static String path="data/";

    //list of edges that we need to analyse line nunmber (13 source callsites)
    static String[] targets={"org.codehaus.mojo.exec.ExecJavaMojo$1.run","org.springframework.boot.loader.MainMethodRunner.run",
            "net.sf.jasperreports.data.hibernate.HibernateDataAdapterService.contributeParameters","org.springframework.boot.maven.RunMojo$LaunchRunner.run",
            "com.google.gwt.dev.shell.ModuleSpace.onLoad","org.apache.maven.surefire.util.ReflectionUtils.invokeMethodWithArray",
            "org.jboss.weld.injection.producer.DefaultLifecycleCallbackInvoker.invokeMethods","com.google.common.eventbus.EventSubscriber.handleEvent",
            "com.google.common.eventbus.EventHandler.handleEvent","com.google.common.base.internal.Finalizer.cleanUp",
            "org.antlr.v4.parse.GrammarTreeVisitor.visit",
            "org.apache.hadoop.hbase.protobuf.ProtobufUtil.toFilter","org.apache.logging.log4j.core.config.plugins.util.PluginBuilder.build"};
    
    static String sourceFile="AllReflectiveCalls.csv";
    
    
    
    public static void main(String[] args) {

        try {

            Map<String,Map<String,List<Integer>>> extractedFromBytecode= new HashMap<>();
            for (int i=0;i<targets.length;i++) {
                Map<String,List<Integer>> versionsMap= new HashMap<>();
               
                extractLineNOFromBytecode(path,targets[i],versionsMap);
                extractedFromBytecode.put(targets[i],versionsMap);
            }

 System.out.println(extractedFromBytecode);
       
            //compute scores
            LineMatcher generator= new LineMatcher(extractedFromBytecode);
            List<Edge> edges=generator.computeMatches(sourceFile);

              
            //produce results
            StringBuilder output= new StringBuilder();
            for(Edge e: edges){
                if(e.getVersions().size()!=0){
                    
                    output.append(e.getSourceCallsite() + "," + e.getSourceCallsiteLineNO() + "," + e.getTargetCallsite() + "," + e.getTargetCallsiteLineNO()
                            + "," + e.getUrl());
                    for (String version : e.getVersions()) {
                        output.append("," + version);
                    }
                    output.append("\n");
                }
                
            }
            BufferedWriter writer=new BufferedWriter( new FileWriter( "scores.csv"));
            writer.write(output.toString());
            writer.close();


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void extractLineNOFromBytecode(String path,String target, Map<String,List<Integer>> versionsMap) throws Exception{
        
        try (Stream<Path> paths = Files.walk(Paths.get(path))) {

            paths.forEach(filePath -> {
                
                if (Files.isRegularFile(filePath)) {
                    try {

                        if (filePath.toString().endsWith(".class")) {
                            String version = filePath.toString().split("/")[2]; //GET VERSIONS :HARD CODED FOR NOW
                            InputStream stream = new FileInputStream(filePath.toFile());
                            

                            List<Integer> versionList=new ArrayList<>();

                            ClassReader reader = new ClassReader(stream);
                            AppClassVisitor cv = new AppClassVisitor(target,versionList);
                            reader.accept(cv, 0);
                            stream.close();
                            //System.out.println("after:"+versionList);
                            if(versionList.size()!=0){
                                versionsMap.put(version,versionList);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
