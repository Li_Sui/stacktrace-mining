package nz.ac.massey.versionAnalyser;

import org.objectweb.asm.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class AppClassVisitor extends ClassVisitor {

    public String className;
    public String targets;

    public  List<Integer> versionList;

    public AppClassVisitor(String targets,List<Integer> versionList) {
        super(Opcodes.ASM5);

        this.targets=targets;
        this.versionList=versionList;
    }

    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        className = name;

    }

    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {

        String fullName=className.replace("/",".")+"."+name;

        if(fullName.equals(targets)){
            return new AppMethodVisitor(fullName,versionList);
        }else{
            return null;
        }

    }

    class AppMethodVisitor extends MethodVisitor {
        int line;
        private String fullname;

        public  List<Integer> versionList;




        public AppMethodVisitor(String fullname, List<Integer> versionList) {
            super(Opcodes.ASM5);
            this.fullname=fullname;
            this.versionList=versionList;

        }

        public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {


            if(name.equals("invoke")){
                versionList.add(line);
               
            }
            super.visitMethodInsn(opcode, owner, name, desc, itf);
        }

        public void visitLineNumber(int line, Label start) {
            this.line = line;
        }


        public void visitEnd() {}
    }

}